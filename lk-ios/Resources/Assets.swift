//
//  UIImage+Assets.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 08.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIImage {

    static var newsActive: UIImage {
        return #imageLiteral(resourceName: "news_active")
    }

    static var newsInactive: UIImage {
        return #imageLiteral(resourceName: "news_inactive")
    }

    static var educationActive: UIImage {
        return #imageLiteral(resourceName: "education_active")
    }

    static var educationInactive: UIImage {
        return #imageLiteral(resourceName: "education_inactive")
    }

    static var announcementActive: UIImage {
        return #imageLiteral(resourceName: "announcement_active")
    }

    static var announcementInactive: UIImage {
        return #imageLiteral(resourceName: "announcement_inactive")
    }

    static var messagesActive: UIImage {
        return #imageLiteral(resourceName: "messages_active")
    }

    static var messagesInactive: UIImage {
        return #imageLiteral(resourceName: "messages_inactive")
    }

    static var profileActive: UIImage {
        return #imageLiteral(resourceName: "profile_active")
    }

    static var profileInactive: UIImage {
        return #imageLiteral(resourceName: "profile_inactive")
    }

    static var writeMessage: UIImage {
        return #imageLiteral(resourceName: "write_message")
    }

    static var logoutIcon: UIImage {
        return #imageLiteral(resourceName: "logout_icon")
    }
}
