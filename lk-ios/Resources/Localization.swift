//
//  Localization.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension String {
    static var newsTitle: String {
        return .localized("news")
    }

    static var announcementsTitle: String {
        return .localized("announcements")
    }

    static var announcementTitle: String {
        return .localized("announcement")
    }

    static var educationTitle: String {
        return .localized("education")
    }

    static var specialtiesTitle: String {
        return .localized("specialties")
    }

    static var semestersTitle: String {
        return .localized("semesters")
    }

    static var messagesTitle: String {
        return .localized("messages")
    }

    static var profileTitle: String {
        return .localized("profile")
    }

    static var contactsTitle: String {
        return .localized("contacts")
    }

    static var gpaBriefTitle: String {
        return .localized("gpa_brief")
    }

    static var gpaTitle: String {
        return .localized("gpa")
    }

    static var errorTitle: String {
        return .localized("error")
    }

    static var gpaErrorMessage: String {
        return .localized("gpa_error_message")
    }

    static var okTitle: String {
        return .localized("ok")
    }

    static var chatInputPlaceholder: String {
        return .localized("Новое сообщение")
    }

    static var profileEmail: String {
        return .localized("profile.email")
    }

    static var profilePhone: String {
        return .localized("profile.phone")
    }

    static var profilePosition: String {
        return .localized("profile.position")
    }

    static var profileBirthDate: String {
        return .localized("profile.birth_date")
    }

    static var saveTitle: String {
        return .localized("save")
    }

    static var deleteTitle: String {
        return .localized("delete")
    }

    static var writeTitle: String {
        return .localized("write")
    }

    static var enterLastNameTitle: String {
        return .localized("enter_last_name")
    }

    static var courseFormat: String {
        return .localized("course_%d")
    }

    static var specialtyTitle: String {
        return .localized("specialty")
    }

    static var studyYearsTitle: String {
        return .localized("years")
    }
}
