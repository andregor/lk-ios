//
//  LoadingIndicator.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

private extension Int {
    static let barsNumber = 5
}

private extension CGFloat {

    static let padding: CGFloat = 2
    static let barHeight: CGFloat = 40
    static let barWidth: CGFloat = 6
}

final class LoadingIndicator: UIView {

    private var bars = [WeakContainer]()
    private weak var containerView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        let width: CGFloat = .barWidth * CGFloat(.barsNumber) + .padding * CGFloat((.barsNumber - 1))
        let containerSize = CGSize(width: width, height: .barHeight)
        containerView.bounds.size = containerSize

        for i in 0 ..< .barsNumber {
            let frame = CGRect(x: CGFloat(i) * (.barWidth + .padding),
                               y: 0,
                               width: .barWidth,
                               height: .barHeight)

            (bars[i].value as! UIView).frame = frame
        }

        containerView.center = center
    }

    func animate() {
        let itemHalfDuration = 0.53 / 2
        let duration = itemHalfDuration * Double(Int.barsNumber)
        let animations = {
            for i in 0 ..< .barsNumber {
                let start = Double(i) * itemHalfDuration / Double(Int.barsNumber)
                let relativeDuration = itemHalfDuration / duration
                let bar = self.bars[i].value as! UIView
                UIView.addKeyframe(withRelativeStartTime: start, relativeDuration: relativeDuration) {
                    bar.transform = CGAffineTransform(scaleX: 1, y: 2.3)
                }

                UIView.addKeyframe(withRelativeStartTime: start + relativeDuration, relativeDuration: relativeDuration) {
                    bar.transform = .identity
                }
            }
        }

        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: [.repeat],
                                animations: animations,
                                completion: nil)
    }

    private func commonInit() {

        let containerView = UIView()
        containerView.clipsToBounds = false

        for _ in 0 ..< .barsNumber {
            let bar = UIView()
            bar.backgroundColor = .lkBlue

            containerView.addSubview(bar)

            bars.append(WeakContainer(value: bar))
        }

        addSubview(containerView)

        self.containerView = containerView
    }
}
