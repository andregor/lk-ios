//
//  CollectionTitleHeaderView.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class CollectionTitleHeaderView: UICollectionReusableView {

    static var reuseIdentifier: String {
        return String(describing: CollectionTitleHeaderView.self)
    }

    private weak var titleLabel: UILabel!

    override var reuseIdentifier: String? {
        return CollectionTitleHeaderView.reuseIdentifier
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func configure(with title: String) {
        titleLabel.text = title
    }

    private func commonInit() {
        let label = UILabel()
        label.textAlignment = .center
        addSubview(label)
        label.pinToSuperview(with: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8))

        titleLabel = label
    }
}
