//
//  LoadingTableFooterView.swift
//  lk-ios
//
//  Created by Egor Snitsar on 20/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class LoadingTableFooterView: UIView {

    private var loadingIndicatorView: UIActivityIndicatorView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        configure()
    }

    private func configure() {
        let loadingView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        addSubview(loadingView)
        loadingView.pinToSuperviewCenter()
        loadingView.startAnimating()
    }
}
