//
//  SeparatorView.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class SeparatorView: UIView {

    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIViewNoIntrinsicMetric, height: 1.0 / UIScreen.main.scale)
    }
}
