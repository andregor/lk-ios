//
//  TableSection.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class TableSection {

    var headerView: UIView?
    var footerView: UIView?
    var headerHeight: CGFloat?
    var footerHeight: CGFloat?
    var boxes: [Box]
    
    init(boxes: [Box]) {
        self.boxes = boxes
    }
}

