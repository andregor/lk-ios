//
//  TableViewDataSource.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class TableViewDataSource: NSObject {

    weak var scrollViewDelegate: UIScrollViewDelegate?

    private weak var tableView: UITableView?
    private var sections = [TableSection]()
    
    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
    }

    var isEmpty: Bool {
        return sections.isEmpty
    }
    
    func update(with boxes: [Box]) {
        let sections = [TableSection(boxes: boxes)]
        update(with: sections)
    }

    func insert(boxes: [Box], at indexPaths: [IndexPath]) {
        precondition(boxes.count == indexPaths.count)

        for (offset, indexPath) in indexPaths.enumerated() {
            sections[indexPath.section].boxes.insert(boxes[offset], at: indexPath.row)
        }

        tableView?.update { $0.insertRows(at: indexPaths, with: .none) }
    }

    func removeBoxes(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation = .none) {
        indexPaths.forEach { indexPath in
            guard indexPath.section < self.sections.count,
                indexPath.row < self.sections[indexPath.section].boxes.count else {
                    return
            }

            sections[indexPath.section].boxes.remove(at: indexPath.row)
        }

        tableView?.deleteRows(at: indexPaths, with: animation)
    }

    func prepend(boxes: [Box]) {
        guard !isEmpty else {
            update(with: boxes)
            return
        }

        guard let section = sections.first else {
            return
        }

        let sectionIndex = sections.index(before: sections.endIndex)
        let startIndex = section.boxes.startIndex
        let endIndex = startIndex + boxes.count
        let indexPahts = (startIndex..<endIndex).map { IndexPath(row: $0, section: sectionIndex) }

        insert(boxes: boxes, at: indexPahts)
    }

    func append(boxes: [Box]) {
        guard !isEmpty else {
            update(with: boxes)
            return
        }

        guard let section = sections.last else {
            return
        }

        let sectionIndex = sections.index(before: sections.endIndex)
        let startIndex = section.boxes.endIndex
        let endIndex = startIndex + boxes.count
        let indexPaths = (startIndex..<endIndex).map { IndexPath(row: $0, section: sectionIndex) }

        insert(boxes: boxes, at: indexPaths)
    }
    
    func update(with sections: [TableSection]) {
        self.sections = sections
        tableView?.reloadData()
    }

    func insert(sections: [TableSection], at indices: IndexSet) {
        precondition(sections.count == indices.count)

        for (index, element) in sections.enumerated() {
            self.sections.insert(element, at: indices[indices.index(indices.startIndex, offsetBy: index)])
        }

        tableView?.update { $0.insertSections(indices, with: .none) }
    }

    func prepend(sections: [TableSection]) {
        let startIndex = self.sections.startIndex
        let endIndex = startIndex + sections.count
        let indexSet = IndexSet(integersIn: startIndex..<endIndex)
        insert(sections: sections, at: indexSet)
    }

    func append(sections: [TableSection]) {
        let startIndex = self.sections.endIndex
        let endIndex = startIndex + sections.count
        let indexSet = IndexSet(integersIn: startIndex..<endIndex)
        insert(sections: sections, at: indexSet)
    }

    func clear() {
        sections = []
        tableView?.reloadData()
    }

    func update(box: Box, at indexPath: IndexPath) {
        sections[indexPath.section].boxes[indexPath.row] = box

        tableView?.update { $0.reloadRows(at: [indexPath], with: .fade) }
    }

    private func box(at indexPath: IndexPath) -> Box {
        return sections[indexPath.section].boxes[indexPath.row]
    }
}

extension TableViewDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].boxes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let box = self.box(at: indexPath)
        tableView.register(cellType: box.cellType)

        let cell = tableView.dequeue(cellType: box.cellType, for: indexPath)
        box.configure(cell: cell)

        return cell
    }
}

extension TableViewDataSource: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return sections[section].headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return sections[section].footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return sections[section].headerHeight ?? sections[section].headerView?.bounds.height ?? .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return sections[section].footerHeight ?? sections[section].footerView?.bounds.height ?? .leastNonzeroMagnitude
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return box(at: indexPath).estimatedHeight ?? tableView.estimatedRowHeight
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return box(at: indexPath).height ?? UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        box(at: indexPath).perform(action: .tap, indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let box = sections[indexPath.section].boxes[indexPath.row]
        guard !box.rowActions.isEmpty else {
            return nil
        }

        return box.rowActions.enumerated().map { [weak tableView] index, rowAction in
            let action = UITableViewRowAction(style: rowAction.style, title: rowAction.title) {
                [weak box] _, _ in

                box?.performRowAction(at: index, indexPath: indexPath)
                tableView?.setEditing(false, animated: true)
            }
            action.backgroundColor = rowAction.backgroundColor
            return action
        }
    }

    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let box = sections[indexPath.section].boxes[indexPath.row]

        guard !box.rowActions.isEmpty else {
            return nil
        }

        let actions: [UIContextualAction] = box.rowActions.enumerated().map { index, rowAction in
            let contextualAction = UIContextualAction(style: rowAction.contextualStyle, title: rowAction.title) {
                [weak box, weak tableView] _, _ ,_ in


                CATransaction.begin()
                CATransaction.setCompletionBlock {
                    box?.performRowAction(at: index, indexPath: indexPath)
                }
                tableView?.setEditing(false, animated: false)
                CATransaction.commit()
            }
            contextualAction.backgroundColor = rowAction.backgroundColor
            return contextualAction
        }

        let configuration = UISwipeActionsConfiguration(actions: actions)
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}

extension TableViewDataSource: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDelegate?.scrollViewDidScroll?(scrollView)
    }
}
