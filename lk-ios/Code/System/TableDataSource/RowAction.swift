//
//  RowAction.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 28.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

struct RowAction {

    enum RowActionType {
        case normal
        case destructive
    }

    let title: String
    let backgroundColor: UIColor

    private let type: RowActionType

    init(title: String, backgroundColor: UIColor, type: RowActionType) {
        self.title = title
        self.backgroundColor = backgroundColor
        self.type = type
    }
}

extension RowAction: Equatable {

    static func == (lhs: RowAction, rhs: RowAction) -> Bool {
        return lhs.title == rhs.title
            && lhs.backgroundColor == rhs.backgroundColor
            && lhs.type == rhs.type
    }
}

extension RowAction {

    @available(iOS 11.0, *)
    var contextualStyle: UIContextualAction.Style {
        switch type {
        case .normal:
            return .normal
        case .destructive:
            return .destructive
        }
    }

    var style: UITableViewRowActionStyle {
        switch type {
        case .normal:
            return .normal
        case .destructive:
            return .destructive
        }
    }
    
}
