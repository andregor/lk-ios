//
//  CellBox.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol Box: class {

    var estimatedHeight: CGFloat? { get }
    var height: CGFloat? { get }

    var cellType: AnyClass { get }

    var rowActions: [RowAction] { get }

    func configure(cell: UITableViewCell)
    func perform(action: ActionType, indexPath: IndexPath)
    func performRowAction(at index: Int, indexPath: IndexPath)
}

final class CellBox<CellType: Configurable> where CellType: UITableViewCell {
    
    typealias Action = (CellType.Model, IndexPath) -> Void
    typealias ContextualAction = (CellType.Model, IndexPath, RowAction) -> Void
    
    private var actions = [ActionType: Action]()
    private var rowActionsMap = [Int: ContextualAction]()
    private(set) var rowActions = [RowAction]()
    
    let viewModel: CellType.Model
    
    init(viewModel: CellType.Model) {
        self.viewModel = viewModel
    }
    
    func with(actionType: ActionType, action: @escaping Action) -> CellBox<CellType> {
        actions[actionType] = action
        return self
    }

    func add(rowAction: RowAction, handler: @escaping ContextualAction) {
        rowActions.append(rowAction)
        rowActionsMap[rowActions.count - 1] = handler
    }

    func remove(rowAction: RowAction) {
        guard let index = rowActions.index(of: rowAction) else {
            return
        }
        rowActions.remove(at: index)
    }
}

extension CellBox: Box {

    var estimatedHeight: CGFloat? {
        return CellType.estimatedHeight
    }

    var height: CGFloat? {
        return CellType.height
    }
    
    var cellType: AnyClass {
        return CellType.self
    }
    
    func configure(cell: UITableViewCell) {
        (cell as? CellType)?.configure(with: viewModel)
    }
    
    func perform(action: ActionType, indexPath: IndexPath) {
        actions[action]?(viewModel, indexPath)
    }

    func performRowAction(at index: Int, indexPath: IndexPath) {
        rowActionsMap[index]?(viewModel, indexPath, rowActions[index])
    }
}
