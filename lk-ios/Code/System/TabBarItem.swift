//
//  TabBarItem.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

enum TabBarItem {
    case news
    case announcements
    case education
    case messages
    case profile
}

extension TabBarItem {

    var title: String {
        switch self {
        case .news:
            return .newsTitle
        case .announcements:
            return .announcementsTitle
        case .education:
            return .educationTitle
        case .messages:
            return .messagesTitle
        case .profile:
            return .profileTitle
        }
    }

    var image: UIImage? {
        switch self {
        case .news:
            return .newsInactive
        case .announcements:
            return .announcementInactive
        case .education:
            return .educationInactive
        case .messages:
            return .messagesInactive
        case .profile:
            return .profileInactive
        }
    }

    var selectedImage: UIImage? {
        switch self {
        case .news:
            return .newsActive
        case .announcements:
            return .announcementActive
        case .education:
            return .educationActive
        case .messages:
            return .messagesActive
        case .profile:
            return .profileActive
        }
    }

    var selectedTitleAttributes: [NSAttributedStringKey: Any] {
        return [.foregroundColor: UIColor.white]
    }
}
