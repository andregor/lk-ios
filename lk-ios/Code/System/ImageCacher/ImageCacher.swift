//
//  ImageCacher.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

/// Class for downloading and caching images
final class ImageCacher {
    
    typealias ImageContainer = UIView & ImageContaining
    
    static let shared = ImageCacher()

    private let cache = NSCache<NSString, UIImage>()
    private let downloader = ImageDownloader()
    
    init(countLimit: Int = 10) {
        cache.countLimit = countLimit
    }

    /// Convenience method for downloading image with placeholder view
    /// - Parameter source: url path for image
    /// - Parameter imageContainer: class that manages image
    func obtainImage(with source: URLConvertible?, for imageContainer: ImageContainer, placeholderView: UIView? = nil) {
        imageContainer.containedImage = nil
        if let placeholder = placeholderView {
            imageContainer.addSubview(placeholder)
        }
        obtain(with: source, for: imageContainer)
    }

    /// Convenience method for downloading image with placeholder image
    /// - Parameter source: url path for image
    /// - Parameter imageContainer: class that manages image
    func obtainImage(with source: URLConvertible?, for imageContainer: ImageContainer, placeholderImage: UIImage? = nil) {
        imageContainer.containedImage = placeholderImage
        obtain(with: source, for: imageContainer)
    }

    /// Performs actual downloading and setting image to imageContainer
    /// - Parameter source: url path for image
    /// - Parameter imageContainer: class that manages image
    private func obtain(with source: URLConvertible?, for imageContainer: ImageContainer) {
        guard let source = source else {
            return
        }

        if let cachedImage = cache.object(forKey: NSString(string: source.asURL().absoluteString)) {
            imageContainer.containedImage = cachedImage
            return
        }
        
        downloader.downloadImage(url: source.asURL()) { [weak imageContainer, weak self] data in
            guard let image = UIImage(data: data) else {
                return
            }

            DispatchQueue.main.async {
                imageContainer?.containedImage = image
            }
            self?.cache.setObject(image, forKey: NSString(string: source.asURL().absoluteString))
        }
    }
}
