//
//  ImageDownloader.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class ImageDownloader {

    private let urlSession = URLSession(configuration: .default)
    
    func downloadImage(url: URL, completion: @escaping (Data) -> Void) {
        let request = URLRequest(url: url)
        
        let dataTask = urlSession.dataTask(with: request) { data, urlResponse, error in
            guard let data = data, error == nil else {
                return
            }
            
            completion(data)
        }
        dataTask.resume()
    }
    
    deinit {
        urlSession.invalidateAndCancel()
    }
}
