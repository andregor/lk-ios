//
//  NavigationController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        updateStyle()
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        topViewController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        super.pushViewController(viewController, animated: animated)
    }

    // MARK: - Private

    private func updateStyle() {
        navigationBar.isTranslucent = false
        navigationBar.tintColor = .white
        navigationBar.shadowImage = UIImage()
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationBar.setBackgroundImage(UIImage(color: .lkBlue), for: .default)
    }
}

extension UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
