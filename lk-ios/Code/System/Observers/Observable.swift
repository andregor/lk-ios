//
//  Observable.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public protocol Observable {

    associatedtype ObserverType

    func add(observer: ObserverType)
    func remove(observer: ObserverType)

    var observers: ObserversArray<ObserverType> { get }
}

public extension Observable {

    public func add(observer: ObserverType) {
        observers.add(observer: observer)
    }

    public func remove(observer: ObserverType) {
        observers.remove(observer: observer)
    }
}
