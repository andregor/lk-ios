//
//  ObserversArray.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public class ObserversArray<T> {

    private var weakPointers = [WeakContainer]()

    func add(observer: T) {
        weakPointers.append(WeakContainer(value: observer as AnyObject))
    }

    func remove(observer: T) {
        for (index, pointer) in weakPointers.enumerated() where pointer.value === (observer as AnyObject) {
            weakPointers.remove(at: index)
        }
    }

    func invoke(_ invocation: (T) -> Void) {
        for pointer in weakPointers.reversed() {
            if let observer = pointer.value {
                invocation(observer as! T)
            } else if let indexToRemove = weakPointers.index(where: { $0 === pointer }) {
                weakPointers.remove(at: indexToRemove)
            }
        }
    }
}

class WeakContainer {

    weak var value: AnyObject?

    init(value: AnyObject) {
        self.value = value
    }
}
