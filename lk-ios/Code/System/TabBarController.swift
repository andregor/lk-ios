//
//  TabBarController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        updateStyle()
    }

    // MARK: - Private

    private func updateStyle() {
        tabBar.tintColor = .white
        tabBar.isTranslucent = false
        tabBar.barTintColor = .lkBlue
    }
}

extension UITabBarController {

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return selectedViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
