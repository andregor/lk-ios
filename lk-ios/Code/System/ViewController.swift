//
//  ViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ViewInput: Stateful {

}

class ViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension ViewController: ViewInput {

    var loadingView: UIView? {
        let view = LoadingIndicator(frame: self.view.bounds)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.animate()
        view.backgroundColor = .white
        return view
    }
}
