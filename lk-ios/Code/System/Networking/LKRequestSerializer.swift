//
//  LKRequestSerializer.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 09.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class LKRequestSerializer: RequestSerializer {

    override func serialize<T>(request: T, for client: HTTPClient) throws -> URLRequest where T : Request {
        var urlRequest = try super.serialize(request: request, for: client)
        
        guard let client = client as? LKClient else {
            return urlRequest
        }
        
        urlRequest.addValue(client.cookieValue, forHTTPHeaderField: "Cookie")
        urlRequest.addValue(client.token, forHTTPHeaderField: "X-XSRF-TOKEN")
        return urlRequest
    }
}

