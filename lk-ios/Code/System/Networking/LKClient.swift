//
//  LKClient.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class LKClient: HTTPClient {

    private(set) var cookieValue: String
    private(set) var token: String
    
    init(configuration: HTTPConfiguration, cookieValue: String = "", token: String = "") {
        self.cookieValue = cookieValue
        self.token = token
        super.init(configuration: configuration)
    }
}

extension LKClient {

    static var client: LKClient {
        let configuration = HTTPConfiguration.lkConfiguration
        let cookies = HTTPCookieStorage.shared.cookies ?? []
        let token = cookies.first(where: { $0.name == "XSRF-TOKEN" })?.value.removingPercentEncoding ?? ""
        
        return LKClient(configuration: configuration, cookieValue: cookies.stringValue, token: token)
    }
}
