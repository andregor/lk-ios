//
//  PagedEntity.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct PagedEntity<T: Decodable>: Decodable {

    var pageSize: Int = 0
    var currentPage: Int = 0
    var totalCount: Int = 0
    var pagesCount: Int = 0

    var items = [T]()

    enum CodingKeys: String, CodingKey {
        case pageSize = "page_size"
        case currentPage = "current_page"
        case totalCount = "total_entries"
        case pagesCount = "total_pages"
    }

    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()

        while !container.isAtEnd {
            if container.currentIndex == 0 {
                let pageContainer = try container.nestedContainer(keyedBy: CodingKeys.self)
                pageSize = try pageContainer.decode(for: .pageSize)
                currentPage = try pageContainer.decode(for: .currentPage)
                totalCount = try pageContainer.decode(for: .totalCount)
                pagesCount = try pageContainer.decode(for: .pagesCount)
            } else {
                items = try container.decode()
            }
        }
    }
}

extension PagedEntity {

    var isLast: Bool {
        return currentPage == pagesCount
    }

    func isItemLast(at index: Int) -> Bool {
        return index == items.index(before: items.endIndex)
    }
}
