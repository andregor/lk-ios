//
//  PagedService.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol PagedService: class {

    associatedtype Request: PagedNetworkRequest

    typealias DataType = PagedEntity<Request.PageEntity>

    var pages: [DataType] { get }

    var currentPage: Int { get }
    var pageSize: Int { get }
    var totalPagesCount: Int { get set }

    var isLoading: Bool { get set }
    var isFullyLoaded: Bool { get }

    var client: LKClient { get }
    var requestCreationClosure: (Int, Int) -> Request { get }

    init(client: LKClient, pageSize: Int)

    func load(refresh: Bool, completion: (() -> Void)?)

    func onLoad(_ page: DataType, refreshed: Bool)
    func onError(_ error: Error)

    func value(at indexPath: IndexPath) -> Request.PageEntity
}

extension PagedService {

    var isFullyLoaded: Bool {
        return currentPage >= totalPagesCount
    }

    var currentPage: Int {
        return pages.count
    }

    var requestCreationClosure: (Int, Int) -> Request {
        return { Request(currentPage: $0, pageSize: $1) }
    }

    func load(refresh: Bool = false, completion: (() -> Void)? = nil) {
        guard (!isLoading && !isFullyLoaded) || refresh else {
            return
        }

        isLoading = true

        let currentPage = refresh ? 1 : pages.count + 1
        let request = requestCreationClosure(currentPage, pageSize)
        client.send(request: request) {
            [weak self] result in

            self?.isLoading = false
            
            switch result {
            case .success(value: let value):
                guard let sself = self, let pagedValue = value as? DataType else {
                    return
                }

                sself.totalPagesCount = pagedValue.pagesCount
                sself.onLoad(pagedValue, refreshed: refresh)

            case .failure(error: let error):
                self?.onError(error)
            }

            completion?()
        }
    }

    func value(at indexPath: IndexPath) -> Request.PageEntity {
        return pages[indexPath.section].items[indexPath.row]
    }
}
