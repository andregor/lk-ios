//
//  PagedRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

protocol PagedNetworkRequest: Request {

    associatedtype PageEntity: Decodable

    var currentPage: Int { get }
    var pageSize: Int { get }

    init(currentPage: Int, pageSize: Int)
}

class PagedRequest<T: Decodable>: PagedNetworkRequest {

    typealias PageEntity = T

    private(set) var currentPage: Int
    private(set) var pageSize: Int

    required init(currentPage: Int, pageSize: Int = 20) {
        self.currentPage = currentPage
        self.pageSize = pageSize
    }

    var method: HTTPMethod {
        return .get
    }

    var urlPath: String {
        fatalError("This is the base class for all paged requests")
    }

    var parameters: [String : Any]? {
        return [
            "current_page": currentPage,
            "page_size": pageSize,
        ]
    }

    var requestHeaders: [String : String] {
        return [:]
    }

    var timeoutInterval: TimeInterval? {
        return nil
    }

    var parametersEncoder: ParametersEncoder {
        return URLParametersEncoder()
    }

    var responseSerializer: JSONResponseSerializer<PagedEntity<PageEntity>> {
        return JSONResponseSerializer()
    }
}
