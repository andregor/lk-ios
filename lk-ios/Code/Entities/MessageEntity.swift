//
//  MessageEntity.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct MessageEntity: Decodable {

    let id: Int
    let text: String
    let isRead: Bool
    let date: Date
    let fromUser: BaseUser
    let toUser: BaseUser

    private enum CodingKeys: String, CodingKey {
        case id
        case text = "content"
        case isRead = "is_read"
        case date
        case fromUser = "from"
        case toUser = "to"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(for: .id)
        text = try container.decode(for: .text)
        date = try container.decode(for: .date)

        isRead = try container.decode(for: .isRead, transform: {
            (decoded: Int) -> Bool in

            return decoded != 0
        })

        fromUser = try container.decode(for: .fromUser)
        toUser = try container.decode(for: .toUser)
    }
}


