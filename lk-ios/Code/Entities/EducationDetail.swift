//
//  EducationDetail.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct EducationDetail: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case briefName = "dv"
        case fullName = "fdv"
        case code
    }

    let id: Int
    let briefName: String
    let fullName: String
    let code: String?
}
