//
//  Grade.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

enum Mark: String, Decodable {

    case great = "Отлично"
    case good = "Хорошо"
    case satisfactory = "Удовлетворительно"
    case failed = "Неудовлетворительно"
    case accepted = "Зачтено"
}

extension Mark {

    var value: Int {
        switch self {
        case .great:
            return 5
        case .good:
            return 4
        case .satisfactory:
            return 3
        case .failed:
            return 2
        case .accepted:
            return 0
        }
    }
}
