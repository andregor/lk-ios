//
//  User.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

class BaseUser: Decodable {

    let id: Int
    let firstName: String
    let lastName: String
    let middleName: String?
    let photoURL: URL?

    var initials: String {
        var result = lastName

        if let name = firstName.firstLetterWithDot {
            result.append(" \(name)")
            result.append(middleName?.firstLetterWithDot ?? "")
        }

        return result
    }

    var fullName: String {
        var name = "\(lastName) \(firstName)"
        if let middleName = middleName {
            name += " \(middleName)"
        }

        return name
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "second_name"
        case middleName = "middle_name"
        case photoURL = "photo_url"
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(for: .id)

        firstName = try container.decode(for: .firstName)
        lastName = try container.decode(for: .lastName)

        photoURL = try container.decode(for: .photoURL, transform: {
            (decoded: String?) -> URL? in

            guard let path = decoded, let baseUrl = URL(string: Services.apiURL) else {
                return nil
            }

            return URL(string: path, relativeTo: baseUrl)
        })

        middleName = try container.decode(for: .middleName, transform: {
            (decoded: String) -> String? in

            return decoded.trimmingCharacters(in: .whitespaces).isEmpty ? nil : decoded
        })
    }

    init(id: Int, firstName: String, lastName: String, middleName: String?, photoURL: URL?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.photoURL = photoURL
    }
}

extension BaseUser: Hashable {

    static func == (lhs: BaseUser, rhs: BaseUser) -> Bool {
        return lhs.id == rhs.id
    }

    var hashValue: Int {
        return id
    }
}

class UserWithEducations: BaseUser {

    let educations: [Education]

    private enum CodingKeys: String, CodingKey {
        case educations
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        educations = try container.decode(for: .educations)

        try super.init(from: decoder)
    }
}

class ExtendedUser: UserWithEducations {

    private enum CodingKeys: String, CodingKey {
        case position
        case phone = "mobile_phone"
        case email = "email"
        case birthDate = "birth_date"
    }

    let position: String
    let phone: String
    let email: String
    let birthDate: Date?

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        position = try container.decode(for: .position)
        phone = try container.decode(for: .phone)
        email = try container.decode(for: .email)
        birthDate = try container.decode(for: .birthDate)

        try super.init(from: decoder)
    }
}

private extension String {

    var firstLetterWithDot: String? {
        guard let firstChar = first else {
            return nil
        }

        return "\(firstChar)."
    }
}
