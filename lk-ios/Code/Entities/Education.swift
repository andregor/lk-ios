//
//  Education.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct Education: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case beginDate = "begda"
        case endDate = "endda"
        case department = "dict_faculties"
        case specialty = "dict_edu_directions"
    }

    let id: Int
    let beginDate: Date
    let endDate: Date
    let department: EducationDetail
    let specialty: EducationDetail
}
