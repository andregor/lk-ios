//
//  Grade.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct Grade: Decodable {

    enum CodingKeys: String, CodingKey {
        case name
        case type
        case mark = "grade"
    }

    let name: String
    let type: String
    let mark: Mark
}
