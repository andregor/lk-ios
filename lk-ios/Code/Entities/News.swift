//
//  News.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct News: Decodable {
    
    let title: String
    let link: String
    let description: String
    let date: Date
    let thumbnail: Thumbnail
}

extension News {

    struct Thumbnail: Decodable {
        
        enum CodingKeys: String, CodingKey {
            case urlString = "url"
            case width
            case height
        }
        
        let urlString: String
        let width: Double
        let height: Double
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            urlString = try container.decode(for: .urlString)

            width = try container.decode(for: .width) {
                (decoded: String) -> Double in

                guard let width = Double(decoded) else {
                    throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.width],
                                                                            debugDescription: "Can't transform String into Double"))
                }

                return width
            }

            height = try container.decode(for: .height) {
                (decoded: String) -> Double in

                guard let height = Double(decoded) else {
                    throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [CodingKeys.height],
                                                                            debugDescription: "Can't transform String into Double"))
                }

                return height
            }
        }
    }
}
