//
//  ContactsQuery.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct NameField: Codable {

    private enum CodingKeys: String, CodingKey {
        case isStrict = "is_strict"
        case needle = "needle"
    }

    let needle: [String]
    let isStrict: Bool

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isStrict, forKey: .isStrict)
        try container.encode(needle, forKey: .needle)
    }
}

struct ContactsQuery: Codable {

    private enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "second_name"
        case middleName = "middle_name"
    }

    let firstName: NameField
    let lastName: NameField
    let middleName: NameField
}

extension NameField {

    static var empty: NameField {
        return NameField(needle: [""], isStrict: false)
    }
}
