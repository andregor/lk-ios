//
//  Session.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct Session: Decodable {

    let name: String
    let grades: [Grade]
}
