//
//  AnnouncementEntity.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct AnnouncementEntity: Decodable {

    let id: Int
    let subject: String
    let content: String
    let date: Date
    let isRead: Bool

    let user: BaseUser

    enum CodingKeys: String, CodingKey {
        case id
        case subject
        case content
        case date
        case isRead = "is_read"
        case user
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(for: .id)
        subject = try container.decode(for: .subject)
        content = try container.decode(for: .content)
        date = try container.decode(for: .date)

        isRead = try container.decode(for: .isRead, transform: {
            (decoded: Int) -> Bool in

            return decoded != 0
        })

        user = try container.decode(for: .user)
    }
}
