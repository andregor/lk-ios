//
//  MessagesRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class MessagesRequest: PagedRequest<MessageEntity> {

    override var urlPath: String {
        return "/api/message"
    }

    override var responseSerializer: JSONResponseSerializer<PagedEntity<MessageEntity>> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiAnnouncementsDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }
}
