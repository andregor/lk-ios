//
//  GradesRequest.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct GradesRequest: Request {

    private let educationId: Int

    init(educationId: Int) {
        self.educationId = educationId
    }

    var urlPath: String {
        return "/api/education/\(educationId)/grades"
    }

    let responseSerializer = JSONResponseSerializer<[GradeGroup]>()
}
