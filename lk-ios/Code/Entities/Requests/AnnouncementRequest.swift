//
//  AnnouncementRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct AnnouncementRequest: Request {

    private let id: Int

    init(id: Int) {
        self.id = id
    }

    var urlPath: String {
        return "/api/user-announcement/\(id)"
    }

    let responseSerializer: JSONResponseSerializer<AnnouncementEntity> = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiAnnouncementsDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }()
}
