//
//  UserRequest.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct UserRequest: Request {

    private let userId: Int

    init(userId: Int) {
        self.userId = userId
    }

    var urlPath: String {
        return "/api/profile/\(userId)"
    }

    let responseSerializer: JSONResponseSerializer<ExtendedUser> = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiUserDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }()
}
