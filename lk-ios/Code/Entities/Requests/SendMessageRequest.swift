//
//  SendMessageRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct SendMessageRequest: Request {

    private let userId: Int
    private let text: String

    init(userId: Int, text: String) {
        self.userId = userId
        self.text = text
    }

    let method: HTTPMethod = .post
    let urlPath = "/api/message/send"
    
    var parametersEncoder: ParametersEncoder {
        return MultipartFormDataParametersEncoder()
    }

    let responseSerializer = VoidSerializer()

    var parameters: [String : Any]? {
        return [
            "to": userId,
            "content": text,
        ]
    }
}
