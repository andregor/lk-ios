//
//  AnnouncementsRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class AnnouncementsRequest: PagedRequest<AnnouncementEntity> {

    override var urlPath: String {
        return "/api/user-announcement"
    }

    override var responseSerializer: JSONResponseSerializer<PagedEntity<AnnouncementEntity>> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiAnnouncementsDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }
}
