//
//  LogoutRequest.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 21.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct LogoutRequest: Request {

    let urlPath = "/logout"

    let responseSerializer = VoidSerializer()
}
