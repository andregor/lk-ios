//
//  NewsRequest.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

struct NewsRequest: Request {

    let urlPath = "/api/news"
    
    let responseSerializer: JSONResponseSerializer<[News]> = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiNewsDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }()
}
