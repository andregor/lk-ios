//
//  MessageHistoryRequest.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class MessageHistoryRequest: PagedRequest<MessageEntity> {

    private var userId: Int = 0

    convenience init(userId: Int, currentPage: Int, pageSize: Int) {
        self.init(currentPage: currentPage, pageSize: pageSize)
        self.userId = userId
    }

    override var urlPath: String {
        return "/api/message/with-user/\(userId)"
    }

    override var responseSerializer: JSONResponseSerializer<PagedEntity<MessageEntity>> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(.apiAnnouncementsDateFormatter)
        return JSONResponseSerializer(decoder: decoder)
    }
}
