//
//  ContactsRequest.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

final class ContactsRequest: PagedRequest<BaseUser> {

    private let query: [String: Any]

    init(lastName: String, currentPage: Int, pageSize: Int = 10) {
        let lastName = NameField(needle: [lastName], isStrict: false)
        let query = ContactsQuery(firstName: .empty, lastName: lastName, middleName: .empty)
        if let params = try? query.toJSON(encoder: JSONEncoder()) {
            self.query = ["query": params as Any]
        } else {
            self.query = [:]
        }

        super.init(currentPage: currentPage, pageSize: pageSize)
    }

    required init(currentPage: Int, pageSize: Int) {
        fatalError("Use another init")
    }

    override var urlPath: String {
        return "/api/contact-list/search"
    }

    override var method: HTTPMethod {
        return .post
    }

    override var parameters: [String : Any]? {
        return super.parameters?.merging(query) { _, new in new}
    }

    override var requestHeaders: [String: String] {
        return ["Content-Type": "application/x-www-form-urlencoded"]
    }

    override var parametersEncoder: ParametersEncoder {
        return URLFormParametersEncoder()
    }

}
