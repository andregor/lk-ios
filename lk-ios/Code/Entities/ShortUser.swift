//
//  ShortUser.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation


class ShortUser: Decodable {

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "second_name"
        case middleName = "middle_name"
        case photoURL = "photo_url"
        case educations
    }

    let id: Int
    let firstName: String
    let lastName: String
    let middleName: String?
    let photoURL: URL?
    let educations: [Education]

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(for: .id)
        educations = try container.decode(for: .educations)

        firstName = try container.decode(for: .firstName)
        lastName = try container.decode(for: .lastName)

        photoURL = try container.decode(for: .photoURL, transform: {
            (decoded: String?) -> URL? in

            guard let path = decoded, let baseUrl = URL(string: Services.apiURL) else {
                return nil
            }

            return URL(string: path, relativeTo: baseUrl)
        })

        middleName = try container.decode(for: .middleName, transform: {
            (decoded: String) -> String? in

            return decoded.trimmingCharacters(in: .whitespaces).isEmpty ? nil : decoded
        })
    }
}

extension ShortUser {

    var fullName: String {
        var result = "\(lastName) \(firstName)"
        if let middleName = middleName {
            result += " \(middleName)"
        }

        return result
    }

    var initials: String {
        var result = lastName

        if let name = firstName.firstLetterWithDot {
            result.append(" \(name)")
            result.append(middleName?.firstLetterWithDot ?? "")
        }

        return result
    }
}

private extension String {

    var firstLetterWithDot: String? {
        guard let firstChar = first else {
            return nil
        }

        return "\(firstChar)."
    }
}
