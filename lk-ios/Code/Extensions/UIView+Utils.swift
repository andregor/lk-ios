//
//  UIView+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 15/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIView {

    private struct Visual {
        static let defaultBorderWidth: CGFloat = 1.0
    }

    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }

    func set(borderColor: UIColor?) {
        set(borderColor: borderColor, borderWidth: Visual.defaultBorderWidth)
    }

    func set(borderColor: UIColor?, borderWidth: CGFloat) {
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = (borderColor != nil ? borderWidth : 0)
    }
}
