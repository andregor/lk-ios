//
//  HTTPConfiguration+Custom.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import HTTPClient

extension HTTPConfiguration {

    static var lkConfiguration: HTTPConfiguration {
        return HTTPConfiguration(baseUrl: "https://lk.etu.ru",
                                 requestSerializer: LKRequestSerializer())
    }
}
