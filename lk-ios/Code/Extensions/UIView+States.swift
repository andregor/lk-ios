//
//  UIView+States.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 18.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIView {

    static func loadingView(frame: CGRect) -> UIView {
        let view = LoadingIndicator(frame: frame)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = .white
        view.animate()
        return view
    }
}
