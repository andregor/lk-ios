//
//  UIView+AutoLayout.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 22.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIView {
    
    func pinToSuperview(with insets: UIEdgeInsets = .zero, excluding edges: UIRectEdge = []) {
        guard let superview = superview else {
            return
        }
        
        let topActive = !edges.contains(.top)
        let leadingActive = !edges.contains(.left)
        let bottomActive = !edges.contains(.bottom)
        let trailingActive = !edges.contains(.right)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if #available(iOS 11, *) {
            topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor, constant: insets.top)
                .isActive = topActive
            leadingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leadingAnchor, constant: insets.left)
                .isActive = leadingActive
            bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor, constant: -insets.bottom)
                .isActive = bottomActive
            trailingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.trailingAnchor, constant: -insets.right)
                .isActive = trailingActive
        } else {
            topAnchor.constraint(equalTo: superview.topAnchor, constant: insets.top).isActive = topActive
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: insets.left).isActive = leadingActive
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -insets.bottom).isActive = bottomActive
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -insets.right).isActive = trailingActive
        }
    }

    func pinToSuperviewCenter() {
        guard let superview = superview else {
            return
        }

        translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            centerXAnchor.constraint(equalTo: superview.centerXAnchor),
            centerYAnchor.constraint(equalTo: superview.centerYAnchor),
            ])
    }

    func systemLayoutSize(fitting width: CGFloat) -> CGSize {
        let widthConstraint = widthAnchor.constraint(equalToConstant: width)
        NSLayoutConstraint.activate([widthConstraint])
        let size = systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        NSLayoutConstraint.deactivate([widthConstraint])

        return size
    }
}
