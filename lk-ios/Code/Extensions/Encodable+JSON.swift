//
//  Encodable+JSON.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension Encodable {

    func toJSON(encoder: JSONEncoder) throws -> [String: Any] {
        let data = try encoder.encode(self)
        guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return [:]
        }

        return json
    }
}
