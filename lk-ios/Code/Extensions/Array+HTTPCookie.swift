//
//  HTTPCookie+Util.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension Array where Element == HTTPCookie {

    var stringValue: String {
        return map {
            $0.name + "=" + $0.value
        }.joined(separator: "; ")
    }
}


