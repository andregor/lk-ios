//
//  UIViewController+TabBarItem.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIViewController {

    func set(tabBarItem: TabBarItem) {
        let item = UITabBarItem(title: tabBarItem.title,
                                image: tabBarItem.image,
                                selectedImage: tabBarItem.selectedImage)
        item.setTitleTextAttributes(tabBarItem.selectedTitleAttributes, for: .selected)
        self.tabBarItem = item
    }
}
