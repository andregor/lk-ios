//
//  UIImageView+ImageCacher.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func setImage(with source: URLConvertible?, placeholderView: UIView? = nil) {
        ImageCacher.shared.obtainImage(with: source, for: self, placeholderView: placeholderView)
    }
    
    func setImage(with source: URLConvertible?, placeholderImage: UIImage? = nil) {
        ImageCacher.shared.obtainImage(with: source, for: self, placeholderImage: placeholderImage)
    }
}

