//
//  UITableVIew+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 20/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UITableView {

    func register(cellType: AnyClass) {
        let cellIdentifier = String(describing: cellType)

        if let nib = UINib(safeWithName: cellIdentifier) {
            register(nib, forCellReuseIdentifier: cellIdentifier)
        } else {
            register(cellType, forCellReuseIdentifier: cellIdentifier)
        }
    }

    func dequeue(cellType: AnyClass) -> UITableViewCell? {
        return dequeueReusableCell(withIdentifier: String(describing: cellType))
    }

    func dequeue(cellType: AnyClass, for indexPath: IndexPath) -> UITableViewCell {
        return dequeueReusableCell(withIdentifier: String(describing: cellType), for: indexPath)
    }

    func update(with block: (UITableView) -> Void) {
        beginUpdates()
        block(self)
        endUpdates()
    }

    func scrollToBottom(animated: Bool = false) {
        scrollRectToVisible(CGRect(x: 0.0, y: contentSize.height - 1.0, width: 1.0, height: 1.0), animated: animated)
    }
}
