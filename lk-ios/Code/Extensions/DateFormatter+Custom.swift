//
//  DateFormatter+Custom.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 09.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension DateFormatter {

    // MARK: - API Formatters

    static let apiNewsDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy, h:mm a"
        formatter.locale = .english
        return formatter
    }()

    static let apiAnnouncementsDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()

    static let apiUserDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    // MARK: - In-App Formatters

    static let inAppEventDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        return formatter
    }()
    
    static let inAppUserDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
}
