//
//  String+Localization.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension String {

    static func localized(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
