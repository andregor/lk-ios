//
//  URL+URLConvertible.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension URL: URLConvertible {

    func asURL() -> URL {
        return self
    }
}
