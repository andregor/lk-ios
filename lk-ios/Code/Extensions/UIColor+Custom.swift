import UIKit

extension UIColor {

    static var lkBlue: UIColor {
        return UIColor(hex: 0x003370)
    }
    
    static var lkGray: UIColor {
        return UIColor(hex: 0x6D6E71)
    }

    static var lkLightBlue: UIColor {
        return UIColor(hex: 0x3374DD)
    }

    static var lkRed: UIColor {
        return UIColor(hex: 0xe02C2C)
    }

    static var lkGreen: UIColor {
        return UIColor(hex: 0x18C10F)
    }
}
