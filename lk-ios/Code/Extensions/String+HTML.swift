//
//  String+HTML.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension String {

    func htmlAttributedString(font: UIFont? = nil, color: UIColor? = nil) -> NSAttributedString? {
        var text = self

        var fontStyleString: String?
        if let font = font {
            fontStyleString = String(format: "font-family: '%@'; font-size: %dpx;", font.fontName, Int(font.pointSize))
        }

        var colorStyleString: String?
        if let color = color {
            colorStyleString = String(format: "color: %@", color.hexString)
        }

        if fontStyleString != nil || colorStyleString != nil {
            text.append(String(format: "<style>*{%@%@}</style>", fontStyleString ?? "", colorStyleString ?? ""))
        }

        guard let data = text.data(using: .utf8) else {
            return nil
        }

        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue,
            ]

        do {
            return try NSMutableAttributedString(data: data, options: options, documentAttributes: nil).removingTrailingWhitespaces()
        } catch {
            return nil
        }
    }
}

private extension NSMutableAttributedString {

    func removingTrailingWhitespaces() -> NSMutableAttributedString {
        self.mutableString.replaceOccurrences(of: "\\s+$",
                                              with: "",
                                              options: .regularExpression,
                                              range: NSRange(location: 0, length: length))

        return self
    }
}
