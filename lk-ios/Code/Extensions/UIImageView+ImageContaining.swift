//
//  UIImageView+ImageContaining.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIImageView: ImageContaining {

    var containedImage: UIImage? {
        get {
            return image
        }
        set {
            image = newValue
        }
    }
}
