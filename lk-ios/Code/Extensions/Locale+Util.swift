//
//  Locale+Util.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 09.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension Locale {

    static var english: Locale {
        return Locale(identifier: "en_US_POSIX")
    }
}
