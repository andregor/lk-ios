//
//  UINib+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 20/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UINib {

    convenience init?(safeWithName name: String, bundle: Bundle = .main) {
        guard bundle.path(forResource: name, ofType: "nib") != nil else {
            return nil
        }

        self.init(nibName: name, bundle: bundle)
    }
}
