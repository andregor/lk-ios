import UIKit

extension UIColor {

    convenience init(hex: UInt32, alpha: CGFloat = 1) {
        let red     = CGFloat((hex & 0xFF0000) >> 16) / 0xFF
        let green   = CGFloat((hex & 0x00FF00) >>  8) / 0xFF
        let blue    = CGFloat((hex & 0x0000FF) >>  0) / 0xFF
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    var hexString: String {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: nil)
        let rgb: UInt32 = (UInt32)(red * 0xFF) << 16 | (UInt32)(green * 0xFF) << 8 | (UInt32)(blue * 0xFF) << 0

        return String(format: "#%06x", rgb)
    }
}
