//
//  UserContacts+Utils.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 20.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import LocalDBClient

extension UserContact {

    init(baseUser: BaseUser) {
        self.init(userId: baseUser.id,
                  firstName: baseUser.firstName,
                  lastName: baseUser.lastName,
                  middleName: baseUser.middleName,
                  photoURL: baseUser.photoURL)

    }
}
