//
//  CGAffineTransform+Util.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension CGAffineTransform {

    static var highlightedCellScale: CGAffineTransform {
        return CGAffineTransform(scaleX: 0.95, y: 0.95)
    }
}
