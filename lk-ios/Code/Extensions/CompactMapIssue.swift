//
//  CompactMapIssue.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension Array {

    #if swift(>=4.1)
    #else
    internal func compactMap<Result>(_ transform: (Element) -> Result?) -> [Result] {
    return flatMap(transform)
    }
    #endif
}

extension Set {

    #if swift(>=4.1)
    #else
    internal func compactMap<Result>(_ transform: (Element) -> Result?) -> [Result] {
    return flatMap(transform)
    }
    #endif
}

extension Dictionary {

    #if swift(>=4.1)
    #else
    internal func compactMap<Result>(_ transform: (Element) -> Result?) -> [Result] {
    return flatMap(transform)
    }
    #endif
}
