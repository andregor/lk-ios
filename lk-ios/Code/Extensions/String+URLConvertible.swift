//
//  String+URLConvertible.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension String: URLConvertible {

    func asURL() -> URL {
        return URL(string: self)!
    }
}
