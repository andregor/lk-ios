//
//  UIImage+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIImage {

    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1.0, height: 1.0)) {
        let rect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContext(size)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        guard let cgImage = image?.cgImage else {
            return nil
        }

        self.init(cgImage: cgImage)
    }
}
