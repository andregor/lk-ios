//
//  UIWindow+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension UIWindow {
    
    enum RootViewControllerTransitionAnimation {
        
        case none
        case crossDissolve
        
        var animationOptions: UIViewAnimationOptions {
            var options: UIViewAnimationOptions = [.allowAnimatedContent]
            switch self {
            case .none:
                break
            case .crossDissolve:
                options.formUnion(.transitionCrossDissolve)
            }
            return options
        }
        
        var isAnimated: Bool {
            return self != .none
        }
    }
    
    func replaceRootViewController(with viewController: UIViewController, animation: RootViewControllerTransitionAnimation, completion: ((Bool) -> Void)?) {
        guard let rootViewController = rootViewController, rootViewController.isViewLoaded, rootViewController.view.layer.presentation() != nil else {
            self.rootViewController = viewController
            return
        }
        
        let onComplete: (Bool) -> Void = { finished in
            if rootViewController.presentedViewController != nil {
                self.dismissPresentedViewControllers(for: rootViewController, completion: {
                    rootViewController.view.removeFromSuperview()
                    completion?(finished)
                })
            } else {
                completion?(finished)
            }
        }
        
        if animation.isAnimated, let rootView = rootViewController.view.snapshotView(afterScreenUpdates: false) {
            self.rootViewController = viewController
            self.rootViewController?.view.addSubview(rootView)
            UIView.transition(with: self, duration: CATransaction.animationDuration(), options: animation.animationOptions, animations: {
                rootView.removeFromSuperview()
            }, completion: onComplete)
        } else {
            self.rootViewController = viewController
            onComplete(true)
        }
    }
    
    private func dismissPresentedViewControllers(for viewController: UIViewController, completion: (() -> ())? = nil) {
        guard viewController.presentedViewController != nil else {
            completion?()
            return
        }
        
        viewController.dismiss(animated: false) {
            [weak self, weak viewController] in
            
            guard let viewController = viewController else {
                return
            }
            
            self?.dismissPresentedViewControllers(for: viewController, completion: completion)
        }
    }
}
