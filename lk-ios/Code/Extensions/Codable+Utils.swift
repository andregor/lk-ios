//
//  Codable+Utils.swift
//  lk-ios
//
//  Created by Egor Snitsar on 15/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension UnkeyedDecodingContainer {

    mutating func decode<T: Decodable>() throws -> T {
        return try decode(T.self)
    }

    mutating func decode<Input: Decodable, Output>(transform: (Input) throws -> Output) throws -> Output {
        let decoded: Input = try decode()

        return try transform(decoded)
    }
}

extension KeyedDecodingContainer {

    func decode<T: Decodable>(for key: KeyedDecodingContainer.Key) throws -> T {
        return try decode(T.self, forKey: key)
    }

    func decode<Input: Decodable, Output>(for key: KeyedDecodingContainer.Key,
                                 transform: (Input) throws -> Output) throws -> Output {
        let decoded: Input = try decode(for: key)

        return try transform(decoded)
    }
}
