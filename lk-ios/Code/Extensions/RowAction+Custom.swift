//
//  RowAction+Custom.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

extension RowAction {

    static var saveAction: RowAction {
        return RowAction(title: .saveTitle, backgroundColor: .lkGreen, type: .normal)
    }

    static var deleteAction: RowAction {
        return RowAction(title: .deleteTitle, backgroundColor: .lkRed, type: .normal)
    }
}
