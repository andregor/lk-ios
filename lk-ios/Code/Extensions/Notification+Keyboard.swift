//
//  Notification+Keyboard.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

extension Notification {

    var keyboardFrame: CGRect? {
        return userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
    }

    var keyboardAnimationDuration: TimeInterval? {
        return userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? TimeInterval
    }

    var keyboardAnimationCurve: UIViewAnimationCurve? {
        return UIViewAnimationCurve(rawValue: userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? Int ?? 0)
    }
}
