//
//  Configurable.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol Configurable {

    associatedtype Model

    static var identifier: String { get }
    static var estimatedHeight: CGFloat? { get }
    static var height: CGFloat? { get }
    
    func configure(with model: Model)
}

extension Configurable {

    static var identifier: String {
        return String(describing: Self.self)
    }

    static var estimatedHeight: CGFloat? {
        return nil
    }

    static var height: CGFloat? {
        return nil
    }
}
