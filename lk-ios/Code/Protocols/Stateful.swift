//
//  Stateful.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

enum State {
    /// Initial loading of data. When failing usually leads to critical error
    case loading
    
    /// Usual working state. Usually follows an intermediate state
    case idle
    
    /// Updating content. Might be used to show loader over the content. When failing usually leads to error
    case updating
    
    /// State for a situation when empty result is received
    case empty
    
    /// Simple error. Usually used to show an error view over the content (e.g. banner or info alert)
    case error(Error)
    
    /// Critical error. Usually used to show an error view insted of the content
    case criticalError(Error)
}

protocol Stateful: class {
    
    var loadingView: UIView? { get }
    var updatingView: UIView? { get }
    var emptyStateView: UIView? { get }
    var criticalErrorView: UIView? { get }
    var errorView: UIView? { get }
    
    func onIdle()
    func onLoading()
    func onUpdating()
    func onEmptyState()
    func onCriticalError(_ error: Error)
    func onError(_ error: Error)
    
    func accept(state: State)
    func hide(stateView: UIView?)
}

extension Stateful {
    
    var loadingView: UIView? {
        return nil
    }
    
    var updatingView: UIView? {
        return nil
    }
    
    var emptyStateView: UIView? {
        return nil
    }
    
    var criticalErrorView: UIView? {
        return nil
    }
    
    var errorView: UIView? {
        return nil
    }
    
    func accept(state: State) {
        switch state {
            
        case .idle:
            onIdle()
            
        case .loading:
            onLoading()
            
        case .updating:
            onUpdating()
            
        case .empty:
            onEmptyState()
            
        case let .error(error):
            onError(error)
            
        case let .criticalError(error):
            onCriticalError(error)
        }
    }
}

extension Stateful where Self: UIViewController {
    
    func onIdle() {
        hide(stateView: UIViewController.currentStateView)
    }
    
    func onLoading() {
        present(view: loadingView)
    }
    
    func onUpdating() {
        present(view: updatingView)
    }
    
    func onEmptyState() {
        present(view: emptyStateView)
    }
    
    func onCriticalError(_ error: Error) {
        present(view: criticalErrorView)
    }
    
    func onError(_ error: Error) {
        present(view: errorView)
    }
    
    func hide(stateView: UIView?) {
        guard let view = stateView else {
            UIViewController.currentStateView?.removeFromSuperview()
            return
        }
        view.removeFromSuperview()
    }
    
    private func present(view: UIView?) {
        guard let view = view else {
            return
        }
        
        hide(stateView: UIViewController.currentStateView)
        self.view.addSubview(view)
        UIViewController.currentStateView = view
    }
}

private extension UIViewController {
    
    static weak var currentStateView: UIView?
}
