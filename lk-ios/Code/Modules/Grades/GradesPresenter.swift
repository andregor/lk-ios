//
//  GradesPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol GradesPresenterOutput: class {

}

final class GradesPresenter {

    weak var view: GradesViewInput?
    var output: GradesPresenterOutput!

    private let session: Session

    init(session: Session) {
        self.session = session
    }
}

// MARK: - View Output

extension GradesPresenter: GradesViewOutput {

    var title: String {
        return session.name
    }

    func viewIsReady() {
        let viewModels = session.grades.map {
            MarkCellViewModel(grade: $0)
        }

        view?.display(marks: viewModels)
    }
}
