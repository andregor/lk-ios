//
//  GradesViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol GradesViewInput: class {
    func display(marks: [MarkCellViewModel])
}

protocol GradesViewOutput {
    var title: String { get }
    func viewIsReady()
}

final class GradesViewController: ViewController {

    var output: GradesViewOutput!

    @IBOutlet private weak var tableView: UITableView!

    private var dataSource: TableViewDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = TableViewDataSource(tableView: tableView)
        title = output.title

        output.viewIsReady()
    }
}

// MARK: - View Input

extension GradesViewController: GradesViewInput {

    func display(marks: [MarkCellViewModel]) {
        let boxes = marks.map {
            CellBox<MarkCell>(viewModel: $0)
        }

        dataSource.update(with: boxes)
    }
}
