//
//  GradesModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol GradesModuleOutput: class {

}

final class GradesModule {

    let storyboard = UIStoryboard(name: "Grades", bundle: nil)

    weak var output: GradesModuleOutput?
    private weak var systemContext: SystemContext?

    func assemble(with systemContext: SystemContext?, session: Session) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? GradesViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = GradesPresenter(session: session)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        return view
    }
}

extension GradesModule: GradesPresenterOutput {

}
