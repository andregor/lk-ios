//
//  AuthenticationModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AuthenticationModuleOutput: class {
    func makeMainModuleRoot()
}

final class AuthenticationModule {

    let storyboard = UIStoryboard(name: "Authentication", bundle: nil)

    weak var output: AuthenticationModuleOutput?
    private weak var systemContext: SystemContext?

    func assemble(with systemContext: SystemContext?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? AuthenticationViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext
        
        let presenter = AuthenticationPresenter()

        view.output = presenter
        presenter.view = view
        presenter.output = self

        return view
    }
}

extension AuthenticationModule: AuthenticationPresenterOutput {

    func showMainModule() {
        output?.makeMainModuleRoot()
    }
}
