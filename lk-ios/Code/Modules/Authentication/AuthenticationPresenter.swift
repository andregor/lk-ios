//
//  AuthenticationPresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol AuthenticationPresenterOutput: class {
    func showMainModule()
}

final class AuthenticationPresenter {

    private struct Constants {
        static let authPageURL = URL(string: "https://lk.etu.ru/login")!
        static let studentPagePath = "https://lk.etu.ru/student"
    }
    
    weak var view: AuthenticationViewInput?
    var output: AuthenticationPresenterOutput!
}

extension AuthenticationPresenter: AuthenticationViewOutput {
    
    func viewIsReady() {
        view?.setURL(Constants.authPageURL)
    }

    func didReceiveRedirect(to url: URL?) {
        guard let url = url, url.absoluteString.hasPrefix(Constants.studentPagePath) else {
            return
        }

        output.showMainModule()
    }
}
