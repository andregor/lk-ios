//
//  AuthenticationViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AuthenticationViewInput: ViewInput {
    func setURL(_ url: URL)
}

protocol AuthenticationViewOutput {
	func viewIsReady()
    func didReceiveRedirect(to url: URL?)
}

final class AuthenticationViewController: ViewController {

    var output: AuthenticationViewOutput!
    
    private lazy var webView: UIWebView = {
        let webView = UIWebView()
        webView.frame = view.bounds
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.delegate = self
        view.addSubview(webView)
        
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        output.viewIsReady()
    }
}

extension AuthenticationViewController: AuthenticationViewInput {
    
    func setURL(_ url: URL) {
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }
}

extension AuthenticationViewController: UIWebViewDelegate {

    func webViewDidStartLoad(_ webView: UIWebView) {
        onLoading()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        onIdle()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        onCriticalError(error)
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        output.didReceiveRedirect(to: request.url)
        return true
    }
}
