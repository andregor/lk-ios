//
//  ProfilePresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct ProfileData {
    let photoURL: URL?
    let name: String
    let info: [TitleSubtitleCellViewModel]
}

protocol ProfilePresenterOutput: class {

    func didLogout()
}

final class ProfilePresenter {

    weak var view: ProfileViewInput?
    var output: ProfilePresenterOutput!

    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }

    private weak var userService: UserService?

    init(userService: UserService?) {
        self.userService = userService
    }
}

extension ProfilePresenter: ProfileViewOutput {

    func viewIsReady() {
        userService?.add(observer: self)
        state = .loading
        userService?.loadCurrentUser()
    }

    func didTapLogout() {
        userService?.logout()
    }
}

extension ProfilePresenter: UserServiceMonitor {

    func didUpdateUser() {
        guard let user = userService?.currentUser else {
            return
        }

        state = .idle
        
        let birthDateString = user.birthDate != nil ? DateFormatter.inAppUserDateFormatter.string(from: user.birthDate!) : ""

        let models = [
            TitleSubtitleCellViewModel(title: .profilePosition, subtitle: user.position),
            TitleSubtitleCellViewModel(title: .profileBirthDate, subtitle: birthDateString),
            TitleSubtitleCellViewModel(title: .profilePhone, subtitle: user.phone),
            TitleSubtitleCellViewModel(title: .profileEmail, subtitle: user.email),
            ]

        let data = ProfileData(photoURL: user.photoURL, name: user.fullName, info: models)

        view?.display(data: data)
    }

    func didLogout() {
        output.didLogout()
    }

    func didFail(with error: Error) {
        state = .criticalError(error)
    }
}
