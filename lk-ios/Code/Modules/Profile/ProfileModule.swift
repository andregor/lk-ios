//
//  ProfileModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ProfileModuleOutput: class {

    func logout()
}

final class ProfileModule {

    let storyboard = UIStoryboard(name: "Profile", bundle: nil)

    weak var output: ProfileModuleOutput?
    private weak var systemContext: SystemContext?

    private weak var rootViewController: NavigationController?

    func assemble(with systemContext: SystemContext?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? ProfileViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = ProfilePresenter(userService: systemContext?.userService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        let navigationController = NavigationController(rootViewController: view)
        rootViewController = navigationController

        return navigationController
    }
}

extension ProfileModule: ProfilePresenterOutput {

    func didLogout() {
        output?.logout()
    }
}
