//
//  ProfileViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ProfileViewInput: ViewInput {
    func display(data: ProfileData)
}

protocol ProfileViewOutput {
    func viewIsReady()
    func didTapLogout()
}

final class ProfileViewController: ViewController {

    var output: ProfileViewOutput!

    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!

    private lazy var dataSource = TableViewDataSource(tableView: self.tableView)

    override func viewDidLoad() {
        super.viewDidLoad()

        title = .profileTitle

        photoImageView.cornerRadius = photoImageView.bounds.height / 2

        let logoutItem = UIBarButtonItem(image: .logoutIcon,
                                         style: .plain,
                                         target: self,
                                         action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = logoutItem

        output.viewIsReady()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let size = headerView.systemLayoutSize(fitting: tableView.bounds.width)
        headerView.frame.size.height = size.height
        tableView.tableHeaderView = headerView
    }

    @objc private func handleLogout() {
        output.didTapLogout()
    }
}

extension ProfileViewController: ProfileViewInput {

    func display(data: ProfileData) {
        if let photoUrl = data.photoURL {
            photoImageView.setImage(with: photoUrl, placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        }

        nameLabel.text = data.name

        let boxes = data.info.map { CellBox<TitleSubtitleCell>(viewModel: $0) }
        dataSource.update(with: boxes)

        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
}
