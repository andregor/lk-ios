//
//  AnnouncementViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AnnouncementViewInput: class {
    func fill(userName: String, userPhotoURL: URL?, dateString: String, text: NSAttributedString)
}

protocol AnnouncementViewOutput {
    func viewIsReady()
}

final class AnnouncementViewController: ViewController {

    private struct Constants {
        static let textViewInsets = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 0.0, right: 16.0)
    }

    var output: AnnouncementViewOutput!

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = .announcementTitle

        userImageView.cornerRadius = userImageView.bounds.height / 2
        textView.textContainerInset = Constants.textViewInsets

        output.viewIsReady()
    }
}

extension AnnouncementViewController: AnnouncementViewInput {

    func fill(userName: String, userPhotoURL: URL?, dateString: String, text: NSAttributedString) {
        userImageView.setImage(with: userPhotoURL, placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        userNameLabel.text = userName
        dateLabel.text = dateString
        textView.attributedText = text
    }
}
