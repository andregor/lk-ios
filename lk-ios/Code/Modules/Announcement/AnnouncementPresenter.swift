//
//  AnnouncementPresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AnnouncementPresenterOutput: class {

}

final class AnnouncementPresenter {

    weak var view: AnnouncementViewInput?
    var output: AnnouncementPresenterOutput!

    private let announcement: AnnouncementEntity
    private let announcementsService: AnnouncementsService?

    init(announcement: AnnouncementEntity, announcementsService: AnnouncementsService?) {
        self.announcement = announcement
        self.announcementsService = announcementsService
    }

    private var dateString: String {
        return DateFormatter.inAppEventDateFormatter.string(from: announcement.date)
    }

    private var announcementText: NSAttributedString {
        let text = NSMutableAttributedString(string: announcement.subject,
                                             attributes: [.font: UIFont.systemFont(ofSize: 20.0, weight: .semibold)])

        if let contentText = announcement.content.htmlAttributedString(font: .systemFont(ofSize: 17.0, weight: .regular), color: .black) {
            text.append(NSAttributedString(string: "\n\n\n"))
            text.append(contentText)
        }

        return text
    }
}

extension AnnouncementPresenter: AnnouncementViewOutput {

    func viewIsReady() {
        announcementsService?.loadAnnouncement(with: announcement.id)

        view?.fill(userName: announcement.user.initials,
                   userPhotoURL: announcement.user.photoURL,
                   dateString: dateString,
                   text: announcementText)
    }
}
