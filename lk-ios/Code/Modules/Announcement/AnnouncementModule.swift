//
//  AnnouncementModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AnnouncementModuleOutput: class {
    
}

final class AnnouncementModule {
    
    let storyboard = UIStoryboard(name: "Announcement", bundle: nil)
    
    weak var output: AnnouncementModuleOutput?
    private weak var systemContext: SystemContext?
    
    func assemble(with announcement: AnnouncementEntity, announcementsService: AnnouncementsService?, systemContext: SystemContext?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? AnnouncementViewController else {
            assert(false)
            return UIViewController()
        }
        
        self.systemContext = systemContext
        
        let presenter = AnnouncementPresenter(announcement: announcement, announcementsService: announcementsService)
        
        view.output = presenter
        presenter.view = view
        presenter.output = self
        
        return view
    }
}

extension AnnouncementModule: AnnouncementPresenterOutput {
    
}
