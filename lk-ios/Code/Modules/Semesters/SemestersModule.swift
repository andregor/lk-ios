//
//  SemestersModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol SemestersModuleOutput: class {

}

final class SemestersModule {

    let storyboard = UIStoryboard(name: "Semesters", bundle: nil)

    weak var output: SemestersModuleOutput?
    private weak var systemContext: SystemContext?
    private weak var rootViewController: UIViewController?

    func assemble(with systemContext: SystemContext?, educationId: Int, gradesService: GradesService) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? SemestersViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = SemestersPresenter(educationId: educationId, gradesService: gradesService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        rootViewController = view

        return view
    }
}

extension SemestersModule: SemestersPresenterOutput {

    func showGradesModule(with session: Session) {
        let module = GradesModule()
        let view = module.assemble(with: systemContext, session: session)
        rootViewController?.navigationController?.pushViewController(view, animated: true)
    }
}
