//
//  SemestersCollectionDataSource.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class SemestersCollectionDataSource: NSObject {

    private weak var collectionView: UICollectionView?
    private(set) var sectionViewModels: [GradeSecionViewModel]

    init(collectionView: UICollectionView, viewModels: [GradeSecionViewModel] = []) {
        self.collectionView = collectionView
        self.sectionViewModels = viewModels
        super.init()

        self.collectionView?.dataSource = self

        self.collectionView?.register(UINib(nibName: SessionCell.identifier, bundle: nil),
                                      forCellWithReuseIdentifier: SessionCell.identifier)

        self.collectionView?.register(CollectionTitleHeaderView.self,
                                      forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                      withReuseIdentifier: CollectionTitleHeaderView.reuseIdentifier)
    }

    func update(with viewModels: [GradeSecionViewModel]) {
        sectionViewModels = viewModels
        collectionView?.reloadData()
    }
}

extension SemestersCollectionDataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SessionCell.identifier, for: indexPath)

        if let cell = cell as? SessionCell {
            let model = sectionViewModels[indexPath.section].sessionViewModels[indexPath.row]
            cell.configure(with: model)
        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {

        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: CollectionTitleHeaderView.reuseIdentifier,
                                                                   for: indexPath)

        if let view = view as? CollectionTitleHeaderView {
            view.configure(with: sectionViewModels[indexPath.section].name)
        }

        return view
    }
}
