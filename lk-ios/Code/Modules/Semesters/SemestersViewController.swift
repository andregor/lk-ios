//
//  SemestersViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

private enum Constants {
    static let sectionsInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    static let headerHeight: CGFloat = 40
}

protocol SemestersViewInput: ViewInput {
    var gpa: Double? { get set }

    func display(gradeSections: [GradeSecionViewModel])
}

protocol SemestersViewOutput {
    func viewIsReady()
    func didSelect(session: Session)
}

final class SemestersViewController: ViewController {

    var output: SemestersViewOutput!

    @IBOutlet private weak var collectionView: UICollectionView!

    private var collectionViewDataSource: SemestersCollectionDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionViewDataSource = SemestersCollectionDataSource(collectionView: collectionView)

        title = .semestersTitle
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: .gpaBriefTitle,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(showGPA))

        output.viewIsReady()
    }

    @objc private func showGPA() {
        let title: String
        let message: String
        if let gpa = gpa {
            title = .gpaTitle
            message = String(format: "%.2f", gpa)
        } else {
            title = .errorTitle
            message = .gpaErrorMessage
        }

        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: .okTitle, style: .default))
        present(alertController, animated: true, completion: nil)
    }

    var gpa: Double?
}

// MARK: - View Input

extension SemestersViewController: SemestersViewInput {

    func display(gradeSections: [GradeSecionViewModel]) {
        collectionViewDataSource.update(with: gradeSections)
    }
}

// MARK: - Collection View Delegate

extension SemestersViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let session = collectionViewDataSource.sectionViewModels[indexPath.section].sessionViewModels[indexPath.row].session else {
            return
        }

        output.didSelect(session: session)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let items = CGFloat(collectionView.numberOfItems(inSection: indexPath.section))
        let padding = Constants.sectionsInsets.left * (items + 1)
        let height = (collectionView.bounds.width - padding) / items
        return CGSize(width: height, height: height)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {

        return Constants.sectionsInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return Constants.sectionsInsets.top
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: collectionView.bounds.width, height: Constants.headerHeight)
    }
}
