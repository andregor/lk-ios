//
//  SemestersPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 15/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol SemestersPresenterOutput: class {
    func showGradesModule(with session: Session)
}

final class SemestersPresenter {

    weak var view: SemestersViewInput?
    var output: SemestersPresenterOutput!

    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }

    private var gpa: Double? {
        didSet {
            view?.gpa = gpa
        }
    }

    private let educationId: Int
    private let gradesService: GradesService

    init(educationId: Int, gradesService: GradesService) {
        self.educationId = educationId
        self.gradesService = gradesService
    }

    private func calculateGPA(groups: [GradeGroup]) {
        let marks = groups.flatMap {
            $0.sessions.flatMap {
                $0.grades.map {
                    $0.mark
                }
            }
        }.filter {
            $0 != .accepted
        }

        let sum = marks.reduce(0) { $0 + $1.value }

        gpa = Double(sum) / Double(marks.count)
    }
}

// MARK: - View Output

extension SemestersPresenter: SemestersViewOutput {

    func viewIsReady() {
        gradesService.add(observer: self)
        state = .loading
        gradesService.getGradeGroups(educationId: educationId)
    }

    func didSelect(session: Session) {
        output.showGradesModule(with: session)
    }
}

// MARK: - Grades Service Monitor

extension SemestersPresenter: GradesServiceMonitor {

    func didLoad(groups: [GradeGroup]) {
        let total = groups.count
        let viewModels: [GradeSecionViewModel] = groups.enumerated().map {
            (index, group) in

            let fallSession: Session
            let springSession: Session?
            if group.sessions.count == 2 {
                fallSession = group.sessions[1]
                springSession = group.sessions[0]
            } else {
                fallSession = group.sessions[0]
                springSession = nil
            }

            let name = String(format: .courseFormat, total - index) + " (\(group.name))"

            return GradeSecionViewModel(name: name, fallSession: fallSession, springSession: springSession)
        }

        DispatchQueue.global().async { [weak self] in
            self?.calculateGPA(groups: groups)
        }

        state = viewModels.isEmpty ? .empty : .idle
        view?.display(gradeSections: viewModels)
    }

    func didFail(error: Error) {
        state = .criticalError(error)
    }
}
