//
//  MessagesModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol MessagesModuleOutput: class {
    
}

final class MessagesModule {
    
    let storyboard = UIStoryboard(name: "Messages", bundle: nil)
    
    weak var output: MessagesModuleOutput?
    private weak var systemContext: SystemContext?
    private weak var userService: UserService?

    private weak var rootViewController: NavigationController?

    func assemble(with systemContext: SystemContext?, messagesService: MessagesService?, userService: UserService?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? MessagesViewController else {
            assert(false)
            return UIViewController()
        }
        
        self.systemContext = systemContext
        self.userService = userService
        
        let presenter = MessagesPresenter(messagesService: messagesService, userService: userService)
        
        view.output = presenter
        presenter.view = view
        presenter.output = self

        let navigationVC = NavigationController(rootViewController: view)
        rootViewController = navigationVC
        
        return navigationVC
    }
}

extension MessagesModule: MessagesPresenterOutput {

    func showChat(with user: BaseUser) {
        let client = systemContext?.httpClient ?? LKClient.client
        let chatService = ChatService(userId: user.id, client: client)
        let module = ChatModule()
        let view = module.assemble(with: systemContext,
                                   chatService: chatService,
                                   userService: userService,
                                   user: user)

        view.hidesBottomBarWhenPushed = true

        rootViewController?.pushViewController(view, animated: true)
    }

    func showContacts() {
        let contactsModule = LocalContactsModule()
        let contactsService = ContactsService(client: systemContext?.httpClient ?? .client)
        let controller = contactsModule.assemble(with: systemContext, contactsService: contactsService)
        controller.hidesBottomBarWhenPushed = true
        contactsModule.output = self

        rootViewController?.pushViewController(controller, animated: true)
    }
}

extension MessagesModule: LocalContactsModuleOutput {

    func contactsShowChat(with user: BaseUser) {
        rootViewController?.popViewController(animated: true)
        showChat(with: user)
    }
}
