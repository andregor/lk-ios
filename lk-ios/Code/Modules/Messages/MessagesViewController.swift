//
//  MessagesViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol MessagesViewInput: ViewInput {
    func display(viewModels: [ChatCellViewModel])
}

protocol MessagesViewOutput {
    func viewIsReady()
    func didTapChat(with user: BaseUser)
    func didTapWrite()
}

final class MessagesViewController: ViewController {

    @IBOutlet private weak var tableView: UITableView!

    private lazy var dataSource = TableViewDataSource(tableView: tableView)

    var output: MessagesViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = .messagesTitle

        let writeItem = UIBarButtonItem(image: .writeMessage, style: .plain, target: self, action: #selector(handleTap))
        navigationItem.rightBarButtonItem = writeItem

        output.viewIsReady()
    }

    @objc private func handleTap() {
        output.didTapWrite()
    }
}

extension MessagesViewController: MessagesViewInput {

    func display(viewModels: [ChatCellViewModel]) {
        let boxes = viewModels.map { box(with: $0) }

        dataSource.update(with: boxes)
    }
    private func box(with viewModel: ChatCellViewModel) -> CellBox<ChatCell> {
        return CellBox<ChatCell>(viewModel: viewModel).with(actionType: .tap, action: {
            [unowned self] viewModel, _ in

            self.output.didTapChat(with: viewModel.user)
        })
    }
}
