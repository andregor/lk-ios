//
//  MessagesPresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol MessagesPresenterOutput: class {
    func showChat(with user: BaseUser)
    func showContacts()
}

final class MessagesPresenter {
    
    weak var view: MessagesViewInput?
    var output: MessagesPresenterOutput!
    
    private let messagesService: MessagesService?
    private let userService: UserService?
    
    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }
    
    init(messagesService: MessagesService?, userService: UserService?) {
        self.messagesService = messagesService
        self.userService = userService
    }
}

extension MessagesPresenter: MessagesViewOutput {
    
    func viewIsReady() {
        messagesService?.add(observer: self)
        userService?.add(observer: self)
        state = .loading
        loadData()
    }

    func didTapChat(with user: BaseUser) {
        output.showChat(with: user)
    }

    func didTapWrite() {
        output.showContacts()
    }

    private func loadData() {
        state = .loading

        let group = DispatchGroup()
        group.enter()
        messagesService?.load {
            group.leave()
        }

        group.enter()
        userService?.loadCurrentUser {
            group.leave()
        }

        group.notify(queue: .main) {
            [weak self] in

            let messages = self?.messagesService?.messages ?? []
            let chats = self?.cellViewModels(from: messages) ?? []

            self?.view?.display(viewModels: chats)
            self?.state = .idle
        }
    }
}

extension MessagesPresenter: MessagesServiceMonitor {
    
    func didLoadMessages() {
        
    }
    
    func didFail(with error: Error) {
        state = .criticalError(error)
    }

    private func cellViewModels(from messages: [MessageEntity]) -> [ChatCellViewModel] {
        guard let currentUser = userService?.currentUser else {
            return []
        }

        let fromUsers = Set<BaseUser>(messages.map { $0.fromUser })
        let toUsers = Set<BaseUser>(messages.map { $0.toUser })
        let allUsers = fromUsers.union(toUsers)

        return allUsers.compactMap({
            user in

            let userMessages: [MessageEntity]
            if user == currentUser {
                userMessages = messages.filter { $0.fromUser == user && $0.toUser == user }
            } else {
                userMessages = messages.filter { $0.fromUser == user || $0.toUser == user }
            }

            let hasUnreadMessages = userMessages.contains { $0.fromUser == user && !$0.isRead }
            guard let lastMessage = userMessages.min(by: { $0.date > $1.date }) else {
                return nil
            }

            return ChatCellViewModel(user: user, message: lastMessage, hasUnreadMessages: hasUnreadMessages)
        }).sorted(by: { $0.message.date > $1.message.date })
    }
}

extension MessagesPresenter: UserServiceMonitor {

    func didUpdateUser() {

    }

    func didLogout() {

    }
}
