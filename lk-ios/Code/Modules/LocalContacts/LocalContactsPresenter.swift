//
//  LocalContactsPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 19/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation
import LocalDBClient

protocol LocalContactsPresenterOutput: class {

    func initiateSearch(query: String)
    func showChat(with user: BaseUser)
}

final class LocalContactsPresenter {

    weak var view: LocalContactsViewInput?
    var output: LocalContactsPresenterOutput!

    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }

    private let localContactsService: LocalContactsService?

    init(localContactsService: LocalContactsService?) {
        self.localContactsService = localContactsService
    }

    private func transform(contacts: [UserData]) -> [ContactCellViewModel] {
        return contacts.map {
            ContactCellViewModel(userId: $0.userId,
                                 imageUrl: $0.photoURL,
                                 lastName: $0.lastName,
                                 firstName: $0.firstName,
                                 middleName: $0.middleName ?? "",
                                 isSaved: true)
        }
    }

    private func reloadData() {
        guard let service = localContactsService else {
            state = .empty
            return
        }

        let viewModels = transform(contacts: service.contacts)
        view?.display(contactViewModels: viewModels)
    }
}

extension LocalContactsPresenter: LocalContactsViewOutput {

    func viewIsReady() {
        localContactsService?.add(observer: self)

        reloadData()
    }

    func didTapSearch(with query: String) {
        output.initiateSearch(query: query)
    }

    func showChat(with viewModel: ContactCellViewModel) {
        let user = BaseUser(id: viewModel.userId,
                            firstName: viewModel.firstName,
                            lastName: viewModel.lastName,
                            middleName: viewModel.middleName,
                            photoURL: viewModel.imageUrl)

        output.showChat(with: user)
    }

    func didToggle(viewModel: ContactCellViewModel) {
        let userContact = UserContact(userId: viewModel.userId,
                                      firstName: viewModel.firstName,
                                      lastName: viewModel.lastName,
                                      middleName: viewModel.middleName,
                                      photoURL: viewModel.imageUrl)
        localContactsService?.toggle(contact: userContact)
    }
}

extension LocalContactsPresenter: LocalContactsServiceMonitor {

    func entityMonitorObservedModifications(entities: [ObservableType]) {
        reloadData()
    }

    func entityMonitorObservedInserts(entities: [ObservableType]) {
        reloadData()
    }

    func entityMonitorObservedDeletions(entities: [ObservableType]) {
        reloadData()
    }
}
