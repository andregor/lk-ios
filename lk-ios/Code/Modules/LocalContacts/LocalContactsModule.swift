//
//  LocalContactsModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 19/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol LocalContactsModuleOutput: class {

    func contactsShowChat(with user: BaseUser)
}

final class LocalContactsModule {

    let storyboard = UIStoryboard(name: "LocalContacts", bundle: nil)

    weak var output: LocalContactsModuleOutput?
    private weak var systemContext: SystemContext?
    private weak var contactsModule: ContactsModule?

    func assemble(with systemContext: SystemContext?, contactsService: ContactsService) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? LocalContactsViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = LocalContactsPresenter(localContactsService: systemContext?.localDBClient?.contactsService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        let contactsModule = ContactsModule()
        let contactsView = contactsModule.assemble(with: systemContext, contactsService: contactsService)
        contactsModule.output = self
        self.contactsModule = contactsModule

        view.searchController = UISearchController(searchResultsController: contactsView)

        return view
    }
}

extension LocalContactsModule: LocalContactsPresenterOutput {

    func initiateSearch(query: String) {
        contactsModule?.initiateSearch(query: query)
    }

    func showChat(with user: BaseUser) {
        output?.contactsShowChat(with: user)
    }
}

extension LocalContactsModule: ContactsModuleOutput {

    func contactsShowChat(with user: BaseUser) {
        output?.contactsShowChat(with: user)
    }
}
