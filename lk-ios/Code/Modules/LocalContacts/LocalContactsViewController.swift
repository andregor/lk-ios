//
//  LocalContactsViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 19/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol LocalContactsViewInput: Stateful {

    func display(contactViewModels: [ContactCellViewModel])
}

protocol LocalContactsViewOutput {
    func viewIsReady()
    func didTapSearch(with query: String)
    func showChat(with viewModel: ContactCellViewModel)
    func didToggle(viewModel: ContactCellViewModel)
}

final class LocalContactsViewController: ViewController {

    var output: LocalContactsViewOutput!

    @IBOutlet private weak var tableView: UITableView!

    var searchController: UISearchController?

    private var dataSource: TableViewDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = .contactsTitle
        configureSearch()

        dataSource = TableViewDataSource(tableView: tableView)
        output.viewIsReady()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let insets: UIEdgeInsets
        if #available(iOS 11, *) {
            insets = view.safeAreaInsets
        } else {
            insets = .zero
        }

        tableView.frame = CGRect(x: insets.left,
                                 y: insets.top,
                                 width: view.bounds.width - insets.left - insets.right,
                                 height: view.bounds.height - insets.top - insets.bottom)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.tableFooterView = UIView()
    }
}

// MARK: - View Input

extension LocalContactsViewController: LocalContactsViewInput {

    func display(contactViewModels: [ContactCellViewModel]) {
        guard !contactViewModels.isEmpty else {
            dataSource.clear()
            return
        }

        let boxes: [Box] = contactViewModels.map {
            let box = CellBox<ContactCell>(viewModel: $0).with(actionType: .tap) {
                [weak self] viewModel, _ in

                self?.output?.showChat(with: viewModel)
            }

            box.add(rowAction: .deleteAction) { [weak self] viewModel, indexPath, _ in
                self?.output.didToggle(viewModel: viewModel)
            }
            return box
        }

        dataSource.update(with: boxes)
    }
}

// MARK: - Search Bar Delegate

extension LocalContactsViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        searchBar.resignFirstResponder()
        output.didTapSearch(with: text)
    }
}

// MARK: - Configuration

private extension LocalContactsViewController {

    func configureSearch() {
        definesPresentationContext = true

        searchController?.searchBar.returnKeyType = .search
        searchController?.searchBar.searchBarStyle = .prominent
        searchController?.searchBar.placeholder = .enterLastNameTitle
        searchController?.searchBar.delegate = self

        tableView.tableHeaderView = searchController?.searchBar
    }
}
