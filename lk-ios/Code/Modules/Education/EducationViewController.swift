//
//  EducationViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol EducationViewInput: ViewInput {
    func display(viewModels: [EducationCellViewModel])
}

protocol EducationViewOutput {
    func viewIsReady()
    func didTapEducation(id: Int)
}

final class EducationViewController: ViewController {

    var output: EducationViewOutput!

    @IBOutlet private weak var tableView: UITableView!

    private var dataSource: TableViewDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = .specialtiesTitle
        dataSource = TableViewDataSource(tableView: tableView)

        output.viewIsReady()
    }
}

// MARK: - View Input

extension EducationViewController: EducationViewInput {

    func display(viewModels: [EducationCellViewModel]) {
        let boxes = viewModels.map {
            CellBox<EducationCell>(viewModel: $0).with(actionType: .tap) {
                [weak self] model, _ in

                self?.output.didTapEducation(id: model.id)
            }
        }

        dataSource.update(with: boxes)
    }
}
