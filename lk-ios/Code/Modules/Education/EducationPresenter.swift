//
//  EducationPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol EducationPresenterOutput: class {

    func showSemesters(educationId: Int)
}

final class EducationPresenter {

    weak var view: EducationViewInput?
    var output: EducationPresenterOutput!
    
    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }
    
    private let userService: UserService
    
    init(userService: UserService) {
        self.userService = userService
    }
}

// MARK: - View Output

extension EducationPresenter: EducationViewOutput {
    
    func viewIsReady() {
        userService.add(observer: self)
        state = .loading
        userService.loadCurrentUser()
    }

    func didTapEducation(id: Int) {
        output.showSemesters(educationId: id)
    }
}

// MARK: - User Service Monitor

extension EducationPresenter: UserServiceMonitor {

    func didUpdateUser() {
        guard let user = userService.currentUser else {
            return
        }
        
        state = .idle
        let educationViewModels: [EducationCellViewModel] = user.educations.map {
            let beginDateString = DateFormatter.inAppUserDateFormatter.string(from: $0.beginDate)
            let endDateString = DateFormatter.inAppUserDateFormatter.string(from: $0.endDate)
            let dateString = "\(beginDateString) - \(endDateString)"
            
            return EducationCellViewModel(id: $0.id,
                                          dateString: dateString,
                                          department: $0.department.fullName,
                                          code: $0.specialty.code ?? "",
                                          specialty: $0.specialty.fullName)
        }
        
        view?.display(viewModels: educationViewModels)
    }
    
    func didFail(with error: Error) {
        state = .criticalError(error)
    }

    func didLogout() {
    }
}
