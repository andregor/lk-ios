//
//  EducationModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol EducationModuleOutput: class {

}

final class EducationModule {

    let storyboard = UIStoryboard(name: "Education", bundle: nil)

    weak var output: EducationModuleOutput?
    private weak var systemContext: SystemContext?

    private weak var rootViewController: NavigationController?

    func assemble(with systemContext: SystemContext?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? EducationViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let client = systemContext?.httpClient ?? LKClient.client
        let userService = systemContext?.userService ?? UserService(client: client)
        let presenter = EducationPresenter(userService: userService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        let navigation = NavigationController(rootViewController: view)
        rootViewController = navigation

        return navigation
    }
}

extension EducationModule: EducationPresenterOutput {

    func showSemesters(educationId: Int) {
        let semestersModule = SemestersModule()
        let gradesService = GradesService(client: systemContext?.httpClient ?? LKClient.client)
        let view = semestersModule.assemble(with: systemContext, educationId: educationId, gradesService: gradesService)
        rootViewController?.pushViewController(view, animated: true)
    }
}
