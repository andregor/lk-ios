//
//  WebViewPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol WebViewPresenterOutput: class {

}

final class WebViewPresenter {

    weak var view: WebViewViewInput?
    var output: WebViewPresenterOutput!
    
    private let url: URL
    private let title: String
    
    init(url: URL, title: String) {
        self.url = url
        self.title = title
    }
}

extension WebViewPresenter: WebViewViewOutput {
    
    func viewIsReady() {
        view?.display(title: title)
        view?.load(request: URLRequest(url: url))
    }
}
