//
//  WebViewModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol WebViewModuleOutput: class {

}

final class WebViewModule {

    let storyboard = UIStoryboard(name: "WebView", bundle: nil)

    weak var output: WebViewModuleOutput?
    private weak var systemContext: SystemContext?

    func assemble(with url: URL, title: String, systemContext: SystemContext? = nil) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? WebViewViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = WebViewPresenter(url: url, title: title)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        return view
    }
}

extension WebViewModule: WebViewPresenterOutput {

}
