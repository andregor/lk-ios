//
//  WebViewViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit
import WebKit

protocol WebViewViewInput: class {

    func display(title: String)
    func load(request: URLRequest)
}

protocol WebViewViewOutput {
    func viewIsReady()
}

final class WebViewViewController: ViewController {

    var output: WebViewViewOutput!
    
    private weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webView = WKWebView()
        view.addSubview(webView)
        webView.pinToSuperview()
        self.webView = webView

        output.viewIsReady()
    }
}

// MARK: - View Input

extension WebViewViewController: WebViewViewInput {

    func display(title: String) {
        self.title = title
    }
    
    func load(request: URLRequest) {
        webView.load(request)
    }
}

// MARK: - WebView Delegate

extension WebViewViewController: WKNavigationDelegate {
}
