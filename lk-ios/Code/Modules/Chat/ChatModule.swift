//
//  ChatModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ChatModuleOutput: class {

}

final class ChatModule {

    let storyboard = UIStoryboard(name: "Chat", bundle: nil)

    weak var output: ChatModuleOutput?
    private weak var systemContext: SystemContext?

    func assemble(with systemContext: SystemContext?,
                  chatService: ChatService?,
                  userService: UserService?,
                  user: BaseUser) -> UIViewController {

        guard let view = storyboard.instantiateInitialViewController() as? ChatViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let presenter = ChatPresenter(user: user, chatService: chatService, userService: userService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        return view
    }
}

extension ChatModule: ChatPresenterOutput {

}
