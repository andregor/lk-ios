//
//  ChatViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ChatViewInput: ViewInput {
    var isFullyLoaded: Bool { get set }
    var message: String? { get set }
    var username: String { get set }
    var isKeyboardHidden: Bool { get set }

    func display(viewModels: [MessageCellViewModel], shouldAppend: Bool)
}

protocol ChatViewOutput {
    func viewIsReady()
    func didScrollToLastMessage()
    func sendMessage(_ message: String)
}

final class ChatViewController: ViewController {

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet weak var toolbarWrapperView: UIView! {
        didSet {
            toolbarWrapperView.backgroundColor = .lkBlue
        }
    }

    @IBOutlet private weak var inputTextField: UITextField! {
        didSet {
            inputTextField.placeholder = .chatInputPlaceholder
            inputTextField.delegate = self
        }
    }

    @IBOutlet private weak var sendButton: UIButton! {
        didSet {
            sendButton.isEnabled = false
            sendButton.addTarget(self, action: #selector(sendMessage), for: .touchUpInside)
        }
    }

    @IBOutlet private weak var toolbarViewBottomConstraint: NSLayoutConstraint!

    private lazy var headerView: LoadingTableFooterView = {
        let view = LoadingTableFooterView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 32.0))
        return view
    }()

    private lazy var dataSource: TableViewDataSource = {
        let dataSource = TableViewDataSource(tableView: self.tableView)
        dataSource.scrollViewDelegate = self

        return dataSource
    }()

    private var keyboardNotificationToken: NSObjectProtocol?

    var output: ChatViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tapRecognizer)

        setupObservers()
        output.viewIsReady()
    }

    deinit {
        keyboardNotificationToken.flatMap { NotificationCenter.default.removeObserver($0) }
    }

    var isFullyLoaded: Bool = false {
        didSet {
            tableView.tableHeaderView = isFullyLoaded ? nil : headerView
        }
    }

    var username: String = "" {
        didSet {
            title = username
        }
    }

    var message: String? {
        get {
            return inputTextField.text
        }
        set {
            inputTextField.text = newValue
            sendButton.isEnabled = (newValue?.count ?? 0) > 0
        }
    }

    var isKeyboardHidden: Bool {
        get {
            return inputTextField.isFirstResponder
        }
        set {
            if isKeyboardHidden {
                inputTextField.resignFirstResponder()
            } else {
                inputTextField.becomeFirstResponder()
            }
        }
    }

    private func setupObservers() {
        keyboardNotificationToken = NotificationCenter.default.addObserver(forName: .UIKeyboardWillChangeFrame, object: nil, queue: nil) {
            [unowned self] note in

            self.handleKeyboardFrame(from: note)
        }
    }

    @objc private func hideKeyboard() {
        view.endEditing(true)
    }


    @objc private func sendMessage() {
        guard let message = inputTextField.text else {
            return
        }

        output.sendMessage(message)
    }

    private func handleKeyboardFrame(from note: Notification) {
        guard let frame = note.keyboardFrame else {
            return
        }

        let convertedRect = view.convert(view.bounds, to: nil)
        let height = convertedRect.intersection(frame).height
        toolbarViewBottomConstraint.constant = height > 0 ? height - bottomLayoutGuide.length : 0

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(note.keyboardAnimationDuration ?? CATransaction.animationDuration())
        UIView.setAnimationCurve(note.keyboardAnimationCurve ?? .linear)
        UIView.setAnimationBeginsFromCurrentState(true)

        view.layoutIfNeeded()
        tableView.scrollToBottom(animated: true)

        UIView.commitAnimations()
    }
}

extension ChatViewController: ChatViewInput {

    func display(viewModels: [MessageCellViewModel], shouldAppend: Bool) {
        guard viewModels.count > 0 else {
            return
        }

        let boxes = viewModels.map { CellBox<MessageCell>(viewModel: $0) }
        if shouldAppend {
            UIView.performWithoutAnimation {
                dataSource.prepend(boxes: boxes)
                tableView.scrollToRow(at: IndexPath(row: boxes.count, section: 0), at: .top, animated: false)
                tableView.contentOffset.y -= headerView.frame.height
            }
        } else {
            dataSource.update(with: boxes)
            tableView.scrollToRow(at: IndexPath(row: boxes.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
}

extension ChatViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text as NSString?
        if let newValue = text?.replacingCharacters(in: range, with: string) {
            sendButton.isEnabled = newValue.count > 0
        }

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage()
        return true
    }
}

extension ChatViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if round(scrollView.contentOffset.y) == 0.0 {
            output.didScrollToLastMessage()
        }
    }
}
