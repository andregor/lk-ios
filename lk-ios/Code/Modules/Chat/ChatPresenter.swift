//
//  ChatPresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol ChatPresenterOutput: class {

}

final class ChatPresenter {

    weak var view: ChatViewInput?
    var output: ChatPresenterOutput!

    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }

    private let user: BaseUser
    private let chatService: ChatService?
    private let userService: UserService?

    init(user: BaseUser, chatService: ChatService?, userService: UserService?) {
        self.user = user
        self.chatService = chatService
        self.userService = userService
        chatService?.add(observer: self)
    }
}

extension ChatPresenter: ChatViewOutput {

    func viewIsReady() {
        view?.username = user.initials
        state = .loading
        reloadMessages()
    }

    func didScrollToLastMessage() {
        chatService?.load()
    }

    func sendMessage(_ message: String) {
        chatService?.send(message: message)
    }

    private func reloadMessages() {
        chatService?.load(refresh: true)
    }
}

extension ChatPresenter: ChatServiceMonitor {

    func didLoad(messagesPage: PagedEntity<MessageEntity>) {
        guard let chatService = chatService, let currentUser = userService?.currentUser else {
            return
        }

        let viewModels = messagesPage.items.map {
            MessageCellViewModel(htmlText: $0.text, isIncoming: $0.toUser == currentUser, date: $0.date)
        }

        state = .idle
        view?.isFullyLoaded = chatService.isFullyLoaded
        view?.display(viewModels: viewModels.reversed(), shouldAppend: chatService.pages.count > 1)
    }

    func didFail(with error: Error) {
        state = chatService?.currentPage == 0 ? .criticalError(error) : .error(error)
    }

    func didUploadMessage(_ message: String) {
        view?.message = nil
        view?.isKeyboardHidden = true
        state = .updating
        reloadMessages()
    }

    func didFailUploadingMessage(_ message: String, error: Error) {
        view?.message = message
    }
}
