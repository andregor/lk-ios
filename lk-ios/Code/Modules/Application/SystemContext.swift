//
//  SystemContext.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation
import LocalDBClient

final class SystemContext {
    let httpClient: LKClient?
    let localDBClient: LocalDBClient?
    let userService: UserService?
    
    init(httpClient: LKClient?, localDBClient: LocalDBClient?, userService: UserService?) {
        self.httpClient = httpClient
        self.localDBClient = localDBClient
        self.userService = userService
    }
}
