//
//  ApplicationModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 22/04/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit
import LocalDBClient

final class ApplicationModule {
    
    private weak var window: UIWindow?
    private var systemContext: SystemContext?
    
    func assemble(with window: UIWindow?) {
        self.window = window
        self.window?.makeKeyAndVisible()
        
        systemContext = SystemContext(httpClient: nil, localDBClient: nil, userService: nil)
    }
    
    func showInitialModule() {
        show(module: AuthenticationModule.self) {
            let module = AuthenticationModule()
            module.output = self
            
            return module.assemble(with: systemContext)
        }
    }
    
    // MARK: - Private
    
    private func show(module: AnyClass, setupClosure: () -> UIViewController) {
        guard let window = window else {
            return
        }
        
        let assembledView = setupClosure()
        
        window.replaceRootViewController(with: assembledView, animation: .crossDissolve, completion: nil)
    }
}

extension ApplicationModule: AuthenticationModuleOutput {

    func makeMainModuleRoot() {
        show(module: TabBarModule.self) {
            let tabBarModule = TabBarModule()
            tabBarModule.output = self
            let client = LKClient.client
            let localDBClient = LocalDBClient()
            systemContext = SystemContext(httpClient: client, localDBClient: localDBClient, userService: UserService(client: client))
            localDBClient.setupCoreDataStack(completion: nil)
            return tabBarModule.assemble(with: systemContext)
        }
    }
}

extension ApplicationModule: TabBarModuleOutput {

    func makeAuthRoot() {
        showInitialModule()
    }
}

struct Services {
    static let apiURL = "https://lk.etu.ru"
}
