//
//  TabBarPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol TabBarPresenterOutput: class {

}

final class TabBarPresenter {

    weak var view: TabBarViewInput?
    var output: TabBarPresenterOutput!
}

extension TabBarPresenter: TabBarViewOutput {
    
    func viewIsReady() {
        
    }
    
}
