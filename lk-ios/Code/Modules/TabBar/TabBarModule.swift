//
//  TabBarModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol TabBarModuleOutput: class {

    func makeAuthRoot()
}

final class TabBarModule {
    
    let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
    
    weak var output: TabBarModuleOutput?
    private weak var systemContext: SystemContext?
    
    private weak var newsModule: NewsModule?
    private weak var educationModule: EducationModule?
    private weak var announcementsModule: AnnouncementsModule?
    private weak var messagesModule: MessagesModule?
    private weak var profileModule: ProfileModule?

    func assemble(with systemContext: SystemContext?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? TabBarViewController else {
            assert(false)
            return UIViewController()
        }
        
        self.systemContext = systemContext
        
        let presenter = TabBarPresenter()
        
        view.output = presenter
        presenter.view = view
        presenter.output = self
        
        let client = systemContext?.httpClient ?? LKClient.client
        let newsService = NewsService(client: client)
        let announcementsService = AnnouncementsService(client: client)
        let messagesService = MessagesService(client: client)
        
        let newsModule = NewsModule()
        newsModule.output = self
        let newsView = newsModule.assemble(with: systemContext, newsService: newsService)
        self.newsModule = newsModule
        
        let educationModule = EducationModule()
        educationModule.output = self
        let educationView = educationModule.assemble(with: systemContext)
        self.educationModule = educationModule
        
        let announcementsModule = AnnouncementsModule()
        announcementsModule.output = self
        let announcementsView = announcementsModule.assemble(with: systemContext, announcementsService: announcementsService)
        self.announcementsModule = announcementsModule
        
        let messagesModule = MessagesModule()
        messagesModule.output = self
        let messagesView = messagesModule.assemble(with: systemContext, messagesService: messagesService, userService: systemContext?.userService)
        self.messagesModule = messagesModule

        let profileModule = ProfileModule()
        profileModule.output = self
        let profileView = profileModule.assemble(with: systemContext)
        self.profileModule = profileModule

        view.setViewControllers([newsView, educationView, announcementsView, messagesView, profileView], animated: false)
        newsView.set(tabBarItem: .news)
        educationView.set(tabBarItem: .education)
        announcementsView.set(tabBarItem: .announcements)
        messagesView.set(tabBarItem: .messages)
        profileView.set(tabBarItem: .profile)

        return view
    }
    
}

// MARK: - TabBar Presenter Output

extension TabBarModule: TabBarPresenterOutput {
    
}

// MARK: - News Module Output

extension TabBarModule: NewsModuleOutput {
    
}

// MARK: - Announcements Module Output

extension TabBarModule: AnnouncementsModuleOutput {
    
}

// MARK: - Education Module Output

extension TabBarModule: EducationModuleOutput {
    
}

// MARK: - Messages Module Output

extension TabBarModule: MessagesModuleOutput {
    
}

// MARK: - Profile Module Output

extension TabBarModule: ProfileModuleOutput {

    func logout() {
        output?.makeAuthRoot()
    }


}
