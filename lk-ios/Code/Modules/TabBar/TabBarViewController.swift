//
//  TabBarViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol TabBarViewInput: class {
    
}

protocol TabBarViewOutput {
	func viewIsReady()
}

final class TabBarViewController: TabBarController {

    var output: TabBarViewOutput!

    override func viewDidLoad() {
        super.viewDidLoad()

        output.viewIsReady()
    }
}

extension TabBarViewController: TabBarViewInput {

}
