//
//  ContactsPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 26/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation
import LocalDBClient

protocol ContactsPresenterOutput: class {
    func showChat(with user: BaseUser)
}

final class ContactsPresenter {

    weak var view: ContactsViewInput?
    var output: ContactsPresenterOutput!

    private var state: State = .idle {
        didSet { view?.accept(state: state) }
    }

    private let contactsService: ContactsService
    private let localContactsService: LocalContactsService?

    init(contactsService: ContactsService, localContactsService: LocalContactsService?) {
        self.contactsService = contactsService
        self.localContactsService = localContactsService
        self.contactsService.add(observer: self)
    }

    private func transform(contacts: [BaseUser]) -> [ContactCellViewModel] {
        return contacts.map {
            let userContact = UserContact(baseUser: $0)
            let saved = self.localContactsService?.hasContact(userContact) ?? false
            return ContactCellViewModel(userId: $0.id,
                                        imageUrl: $0.photoURL,
                                        lastName: $0.lastName,
                                        firstName: $0.firstName,
                                        middleName: $0.middleName ?? "",
                                        isSaved: saved)
        }
    }

    private func reloadData() {
        let contacts = contactsService.pages.flatMap {
            $0.items
        }
        view?.display(contactViewModels: transform(contacts: contacts), reload: true)
    }
}

extension ContactsPresenter: ContactsViewOutput {

    func viewIsReady() {
        localContactsService?.add(observer: self)
    }

    func didInitiateSearch(query: String) {
        state = .loading
        contactsService.load(lastName: query, refresh: true)
    }

    func didScrollToBottom() {
        contactsService.load()
    }

    func showChat(with viewModel: ContactCellViewModel) {
        guard let user = contactsService.user(with: viewModel.userId) else {
            return
        }

        output.showChat(with: user)
    }

    func didToggle(viewModel: ContactCellViewModel) {
        let userContact = UserContact(userId: viewModel.userId,
                                      firstName: viewModel.firstName,
                                      lastName: viewModel.lastName,
                                      middleName: viewModel.middleName,
                                      photoURL: viewModel.imageUrl)
        localContactsService?.toggle(contact: userContact)
    }
}

extension ContactsPresenter: ContactsServiceMonitor {

    func didLoad(contacts: [BaseUser], refreshed: Bool) {
        guard !contacts.isEmpty || !contactsService.pages.isEmpty else {
            state = .empty
            return
        }

        if refreshed {
            view?.display(contactViewModels: [], reload: false)
        }

        state = .idle
        view?.display(contactViewModels: transform(contacts: contacts), reload: false)
        view?.isFullyLoaded = contactsService.isFullyLoaded
    }

    func didFail(error: Error) {
        if contactsService.pages.isEmpty {
            state = .criticalError(error)
        } else {
            state = .error(error)
        }
    }
}

extension ContactsPresenter: LocalContactsServiceMonitor {

    func entityMonitorObservedModifications(entities: [ObservableType]) {
        reloadData()
    }

    func entityMonitorObservedInserts(entities: [ObservableType]) {
        reloadData()
    }

    func entityMonitorObservedDeletions(entities: [ObservableType]) {
        reloadData()
    }
}
