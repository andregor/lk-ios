//
//  ContactsModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 26/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ContactsModuleOutput: class {
    func contactsShowChat(with user: BaseUser)
}

final class ContactsModule {

    let storyboard = UIStoryboard(name: "Contacts", bundle: nil)

    weak var output: ContactsModuleOutput?
    private weak var systemContext: SystemContext?
    private weak var presenter: ContactsPresenter?

    func assemble(with systemContext: SystemContext?, contactsService: ContactsService) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? ContactsViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext

        let localContactsService = systemContext?.localDBClient?.contactsService
        let presenter = ContactsPresenter(contactsService: contactsService, localContactsService: localContactsService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        self.presenter = presenter

        return view
    }

    func initiateSearch(query: String) {
        presenter?.didInitiateSearch(query: query)
    }
}

extension ContactsModule: ContactsPresenterOutput {

    func showChat(with user: BaseUser) {
        output?.contactsShowChat(with: user)
    }
}
