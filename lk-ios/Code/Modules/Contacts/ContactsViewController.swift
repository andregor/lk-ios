//
//  ContactsViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 26/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol ContactsViewInput: Stateful {
    var isFullyLoaded: Bool { get set }

    func display(contactViewModels: [ContactCellViewModel], reload: Bool)
}

protocol ContactsViewOutput {
    func viewIsReady()
    func didScrollToBottom()
    func showChat(with viewModel: ContactCellViewModel)
    func didInitiateSearch(query: String)
    func didToggle(viewModel: ContactCellViewModel)
}

final class ContactsViewController: ViewController {

    var output: ContactsViewOutput!
    
    @IBOutlet private weak var tableView: UITableView!

    private var dataSource: TableViewDataSource!

    var isFullyLoaded = false {
        didSet {
            tableView.tableFooterView = isFullyLoaded ? nil : footerView
        }
    }

    private lazy var footerView: LoadingTableFooterView = {
        let view = LoadingTableFooterView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 32.0))
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        tableView.pinToSuperview()
        self.tableView = tableView

        dataSource = TableViewDataSource(tableView: tableView)

        dataSource.scrollViewDelegate = self

        edgesForExtendedLayout = []

        output.viewIsReady()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        tableView.tableFooterView = UIView()
        dataSource.clear()
    }
}

// MARK: - View Input

extension ContactsViewController: ContactsViewInput {

    func display(contactViewModels: [ContactCellViewModel], reload: Bool) {
        guard !contactViewModels.isEmpty else {
            dataSource.clear()
            return
        }

        let boxes: [Box] = contactViewModels.map {
            let rowAction: RowAction = $0.isSaved ? .deleteAction : .saveAction
            let box = CellBox<ContactCell>(viewModel: $0).with(actionType: .tap) {
                [weak self] viewModel, _ in

                self?.output?.showChat(with: viewModel)
            }

            box.add(rowAction: rowAction) { [weak self] viewModel, _, _ in
                viewModel.toggleSaved()
                self?.output.didToggle(viewModel: viewModel)
            }
            return box
        }

        if reload {
            dataSource.update(with: boxes)
        } else {
            dataSource.append(boxes: boxes)
        }

    }
}

// MARK: - Stateful

extension ContactsViewController {
    
}

extension ContactsViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if round(scrollView.contentSize.height) == round(scrollView.contentOffset.y + scrollView.frame.height),
            !isFullyLoaded {
            output?.didScrollToBottom()
        }
    }
}
