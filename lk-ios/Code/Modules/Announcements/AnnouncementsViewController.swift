//
//  AnnouncementsViewController.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AnnouncementsViewInput: ViewInput {
    var isFullyLoaded: Bool { get set }

    func display(viewModels: [AnnouncementCellViewModel], shouldAppend: Bool)
    func update(viewModel: AnnouncementCellViewModel, at indexPath: IndexPath)
}

protocol AnnouncementsViewOutput {
    func viewIsReady()
    func didScrollToBottom()
    func didSelectAnnouncement(at indexPath: IndexPath)
}

final class AnnouncementsViewController: ViewController {

    var output: AnnouncementsViewOutput!

    private lazy var dataSource: TableViewDataSource = {
        let dataSource = TableViewDataSource(tableView: self.tableView)
        dataSource.scrollViewDelegate = self

        return dataSource
    }()

    @IBOutlet private weak var tableView: UITableView!

    private lazy var footerView: LoadingTableFooterView = {
        let view = LoadingTableFooterView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 32.0))
        return view
    }()

    var isFullyLoaded: Bool = false {
        didSet {
            tableView.tableFooterView = isFullyLoaded ? nil : footerView
        }
    }

    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        title = .announcementsTitle

        output.viewIsReady()
    }
}

extension AnnouncementsViewController: AnnouncementsViewInput {

    func display(viewModels: [AnnouncementCellViewModel], shouldAppend: Bool) {
        let boxes = viewModels.map { box(with: $0) }
        let sections = [TableSection(boxes: boxes)]

        if shouldAppend {
            dataSource.append(sections: sections)
        } else {
            dataSource.update(with: sections)
        }
    }

    func update(viewModel: AnnouncementCellViewModel, at indexPath: IndexPath) {
        dataSource.update(box: box(with: viewModel), at: indexPath)
    }

    private func box(with viewModel: AnnouncementCellViewModel) -> CellBox<AnnouncementCell> {
        return CellBox<AnnouncementCell>(viewModel: viewModel).with(actionType: .tap, action: {
            [weak self] _, indexPath in

            self?.output.didSelectAnnouncement(at: indexPath)
        })
    }
}

extension AnnouncementsViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if round(scrollView.contentSize.height) == round(scrollView.contentOffset.y + scrollView.frame.height),
            !isFullyLoaded {
            output.didScrollToBottom()
        }
    }
}
