//
//  AnnouncementsPresenter.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol AnnouncementsPresenterOutput: class {
    func showAnnouncement(_ announcement: AnnouncementEntity)
}

final class AnnouncementsPresenter {

    weak var view: AnnouncementsViewInput?
    var output: AnnouncementsPresenterOutput!

    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }

    private let announcementsService: AnnouncementsService?

    init(announcementsService: AnnouncementsService?) {
        self.announcementsService = announcementsService
    }

    private func cellViewModel(page: PagedEntity<AnnouncementEntity>, announcement: AnnouncementEntity, index: Int) -> AnnouncementCellViewModel {
        let isLast = page.isLast && page.isItemLast(at: index)

        return AnnouncementCellViewModel(subject: announcement.subject,
                                         date: announcement.date,
                                         isRead: announcement.isRead,
                                         userName: announcement.user.initials,
                                         isLast: isLast)
    }
}

extension AnnouncementsPresenter: AnnouncementsServiceMonitor {

    func didLoad(announcementsPage: PagedEntity<AnnouncementEntity>) {
        guard let service = announcementsService, !service.pages.isEmpty else {
            state = .empty
            return
        }

        let models = announcementsPage.items.enumerated().map {
            params -> AnnouncementCellViewModel in

            return cellViewModel(page: announcementsPage, announcement: params.element, index: params.offset)
        }

        state = .idle
        view?.display(viewModels: models, shouldAppend: service.pages.count > 1)
        view?.isFullyLoaded = service.isFullyLoaded
    }

    func didLoad(announcement: AnnouncementEntity) {
        guard let indexPath = announcementsService?.indexPath(for: announcement),
            let page = announcementsService?.pages[indexPath.section] else {
                return
        }

        let model = cellViewModel(page: page, announcement: announcement, index: indexPath.row)
        view?.update(viewModel: model, at: indexPath)
    }

    func didFail(with error: Error) {
        state = announcementsService?.currentPage == 0 ? .criticalError(error) : .error(error)
    }
}

extension AnnouncementsPresenter: AnnouncementsViewOutput {

    func viewIsReady() {
        announcementsService?.add(observer: self)
        state = .loading
        announcementsService?.load(refresh: true)
    }

    func didSelectAnnouncement(at indexPath: IndexPath) {
        guard let model = announcementsService?.value(at: indexPath) else {
            return
        }

        output.showAnnouncement(model)
    }

    func didScrollToBottom() {
        announcementsService?.load()
    }
}
