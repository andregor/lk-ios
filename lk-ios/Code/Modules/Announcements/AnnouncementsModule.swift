//
//  AnnouncementsModule.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol AnnouncementsModuleOutput: class {

}

final class AnnouncementsModule {

    let storyboard = UIStoryboard(name: "Announcements", bundle: nil)

    weak var output: AnnouncementsModuleOutput?

    private weak var systemContext: SystemContext?
    private weak var announcementsService: AnnouncementsService?

    private weak var rootViewController: NavigationController?

    func assemble(with systemContext: SystemContext?, announcementsService: AnnouncementsService?) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? AnnouncementsViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext
        self.announcementsService = announcementsService

        let presenter = AnnouncementsPresenter(announcementsService: announcementsService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        let navigationVC = NavigationController(rootViewController: view)
        rootViewController = navigationVC

        return navigationVC
    }
}

extension AnnouncementsModule: AnnouncementsPresenterOutput {

    func showAnnouncement(_ announcement: AnnouncementEntity) {
        let module = AnnouncementModule()
        let view = module.assemble(with: announcement, announcementsService: announcementsService, systemContext: systemContext)

        rootViewController?.pushViewController(view, animated: true)
    }
}
