//
//  NewsPresenter.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol NewsPresenterOutput: class {

    func showNewsDetail(url: URL, title: String)
}

final class NewsPresenter {

    weak var view: NewsViewInput?
    var output: NewsPresenterOutput!
    
    private var state: State = .idle {
        didSet {
            view?.accept(state: state)
        }
    }
    
    private let newsService: NewsService
    
    init(newsService: NewsService) {
        self.newsService = newsService
    }
}

// MARK: - News View Output

extension NewsPresenter: NewsViewOutput {
    
    func viewIsReady() {
        newsService.add(observer: self)
        state = .loading
        newsService.getNews()
    }
}

extension NewsPresenter: NewsServiceMonitor {

    func didLoad(news: NewsService.DataType) {
        state = news.isEmpty ? .empty : .idle
        transform(models: news)
    }

    func didFail(with error: Error) {
        state = .criticalError(error)
    }
    
    func didTapNews(newsPath: String, newsTitle: String) {
        guard let url = URL(string: newsPath) else {
            return
        }
        output.showNewsDetail(url: url, title: newsTitle)
    }
}

// MARK: - Private

private extension NewsPresenter {
    
    func transform(models: [News]) {
        guard !models.isEmpty else {
            return
        }

        let viewModels = models.map {
            NewsCellViewModel(imagePath: $0.thumbnail.urlString, title: $0.title, subtitle: $0.description, newsPath: $0.link)
        }
        
        view?.display(viewModels: viewModels)
    }
}
