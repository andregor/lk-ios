//
//  NewsModule.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol NewsModuleOutput: class {
}

final class NewsModule {

    let storyboard = UIStoryboard(name: "News", bundle: nil)

    weak var output: NewsModuleOutput?

    private weak var systemContext: SystemContext?
    private weak var newsService: NewsService?

    private weak var rootViewController: NavigationController?

    func assemble(with systemContext: SystemContext?, newsService: NewsService) -> UIViewController {
        guard let view = storyboard.instantiateInitialViewController() as? NewsViewController else {
            assert(false)
            return UIViewController()
        }

        self.systemContext = systemContext
        self.newsService = newsService

        let presenter = NewsPresenter(newsService: newsService)

        view.output = presenter
        presenter.view = view
        presenter.output = self

        let navigationVC = NavigationController(rootViewController: view)
        rootViewController = navigationVC
        
        return navigationVC
    }
}

extension NewsModule: NewsPresenterOutput {

    func showNewsDetail(url: URL, title: String) {
        let webViewModule = WebViewModule()
        let webViewController = webViewModule.assemble(with: url, title: title)
        rootViewController?.pushViewController(webViewController, animated: true)
    }
}
