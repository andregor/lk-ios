//
//  NewsViewController.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

protocol NewsViewInput: ViewInput {
    func display(viewModels: [NewsCellViewModel])
}

protocol NewsViewOutput {
	func viewIsReady()
    func didTapNews(newsPath: String, newsTitle: String)
}

final class NewsViewController: ViewController {

    var output: NewsViewOutput!
    
    private var dataSource: TableViewDataSource!

    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Initialization

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        title = .newsTitle
    }

    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = TableViewDataSource(tableView: tableView)

        output.viewIsReady()
    }
}

// MARK: - View Input

extension NewsViewController: NewsViewInput {

    func display(viewModels: [NewsCellViewModel]) {
        let sections: [TableSection] = viewModels.map {
            let box = CellBox<NewsCell>(viewModel: $0).with(actionType: .tap) {
                [weak self] model, _ in

                self?.output.didTapNews(newsPath: model.newsPath, newsTitle: model.title)
            }

            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
            let section = TableSection(boxes: [box])
            section.footerView = footerView
            return section
        }
        
        dataSource.update(with: sections)
    }
}
