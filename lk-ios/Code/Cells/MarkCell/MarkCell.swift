//
//  MarkCell.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class MarkCell: UITableViewCell {
    
    @IBOutlet private weak var subjectLabel: UILabel!
    @IBOutlet private weak var subjectTypeLabel: UILabel!
    @IBOutlet private weak var markLabel: UILabel!
}

extension MarkCell: Configurable {

    func configure(with model: MarkCellViewModel) {
        subjectLabel.text = model.subject
        subjectTypeLabel.text = model.subjectType
        markLabel.text = model.mark
    }
}
