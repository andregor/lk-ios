//
//  MarkCellViewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 17.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class MarkCellViewModel {

    let subject: String
    let subjectType: String
    let mark: String

    init(grade: Grade) {
        subject = grade.name
        subjectType = grade.type
        mark = grade.mark.rawValue
    }
}
