//
//  ChatCell.swift
//  lk-ios
//
//  Created by Egor Snitsar on 07/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet private weak var userImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        userImageView.cornerRadius = userImageView.bounds.width / 2
    }
}

extension ChatCell: Configurable {

    private struct Visual {
        static let unreadBorderWidth: CGFloat = 2.0
    }

    func configure(with model: ChatCellViewModel) {
        userImageView.setImage(with: model.photoURL, placeholderImage: #imageLiteral(resourceName: "user_placeholder"))
        userImageView.set(borderColor: model.hasUnreadMessages ? .lkBlue : nil, borderWidth: Visual.unreadBorderWidth)
        nameLabel.text = model.name
        contentLabel.attributedText = model.content
        contentLabel.lineBreakMode = .byTruncatingTail
        dateLabel.text = model.dateString
    }
}
