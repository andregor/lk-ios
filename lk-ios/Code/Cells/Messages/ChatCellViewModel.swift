//
//  ChatCellViewModel.swift
//  lk-ios
//
//  Created by Egor Snitsar on 07/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct ChatCellViewModel {

    let user: BaseUser
    let message: MessageEntity
    let hasUnreadMessages: Bool

    var photoURL: URL? {
        return user.photoURL
    }

    var name: String {
        return user.initials
    }

    var content: NSAttributedString? {
        return message.text.htmlAttributedString(font: .systemFont(ofSize: 14.0), color: .lightGray)
    }

    var dateString: String {
        return DateFormatter.inAppEventDateFormatter.string(from: message.date)
    }

    init(user: BaseUser, message: MessageEntity, hasUnreadMessages: Bool) {
        self.user = user
        self.message = message
        self.hasUnreadMessages = hasUnreadMessages
    }
}
