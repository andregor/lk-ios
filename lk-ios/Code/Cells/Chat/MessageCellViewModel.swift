//
//  MessageCellViewModel.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class MessageCellViewModel {

    let text: NSAttributedString?
    let dateString: String
    let isIncoming: Bool

    init(htmlText: String, isIncoming: Bool, date: Date) {
        self.text = htmlText.htmlAttributedString(font: .systemFont(ofSize: 14.0), color: isIncoming ? .black : .white)
        self.dateString = DateFormatter.inAppEventDateFormatter.string(from: date)
        self.isIncoming = isIncoming
    }
}
