//
//  MessageCell.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet private weak var messageContentView: UIView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!

    @IBOutlet private var outcomingConstraints: [NSLayoutConstraint]!
    @IBOutlet private var incomingConstraints: [NSLayoutConstraint]!

    private var isIncoming: Bool = false {
        didSet {
            setNeedsUpdateConstraints()
            messageContentView.backgroundColor = isIncoming ? #colorLiteral(red: 0.8862745098, green: 0.8862745098, blue: 0.9058823529, alpha: 1) : #colorLiteral(red: 0.1294117647, green: 0.5490196078, blue: 0.9607843137, alpha: 1)
            messageContentView.cornerRadius = 16.0
        }
    }

    override func updateConstraints() {
        super.updateConstraints()

        NSLayoutConstraint.deactivate([incomingConstraints, outcomingConstraints].flatMap({ $0 }))
        NSLayoutConstraint.activate(isIncoming ? incomingConstraints : outcomingConstraints)
    }
}

extension MessageCell: Configurable {

    func configure(with model: MessageCellViewModel) {
        isIncoming = model.isIncoming
        contentLabel.attributedText = model.text
        dateLabel.text = model.dateString
    }
}
