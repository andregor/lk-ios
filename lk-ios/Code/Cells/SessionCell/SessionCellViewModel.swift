//
//  SessionCellViewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class SessionCellViewModel {

    let session: Session?

    init(session: Session?) {
        self.session = session
    }
}

extension SessionCellViewModel {

    var title: String {
        return session?.name ?? ""
    }

    var isEmpty: Bool {
        return session == nil
    }
}
