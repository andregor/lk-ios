//
//  SessionCell.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class SessionCell: UICollectionViewCell {

    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var emptyImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!

    override var isHighlighted: Bool {
        didSet {
            let transform: CGAffineTransform = isHighlighted ? .highlightedCellScale : .identity
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                self.containerView.transform = transform
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        emptyImageView.isHidden = true

        containerView.layer.shadowOffset = CGSize(width: 1, height: 2)
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.cornerRadius = 5
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()

        containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        emptyImageView.isHidden = true
        titleLabel.isHidden = false
    }
    
}

extension SessionCell: Configurable {

    func configure(with model: SessionCellViewModel) {
        guard !model.isEmpty else {
            titleLabel.isHidden = true
            emptyImageView.isHidden = false
            return
        }

        titleLabel.text = model.title
    }
}
