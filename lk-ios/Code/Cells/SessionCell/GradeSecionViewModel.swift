//
//  GradeGroupViewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class GradeSecionViewModel {

    let name: String
    let sessionViewModels: [SessionCellViewModel]

    init(name: String, fallSession: Session?, springSession: Session?) {
        self.name = name
        sessionViewModels = [SessionCellViewModel(session: fallSession),
                             SessionCellViewModel(session: springSession)]
    }
}
