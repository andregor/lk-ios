//
//  EducationCellViewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class EducationCellViewModel {

    let id: Int
    let dateString: String
    let department: String
    let code: String
    let specialty: String
    
    init(id: Int, dateString: String, department: String, code: String, specialty: String) {
        self.id = id
        self.dateString = dateString
        self.department = department
        self.code = code
        self.specialty = specialty
    }
}
