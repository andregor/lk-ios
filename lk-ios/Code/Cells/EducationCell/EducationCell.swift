//
//  EducationCell.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 13.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class EducationCell: UITableViewCell {
    
    @IBOutlet private weak var dateTitleLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var departmentLabel: UILabel!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var specialtyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.shadowOffset = CGSize(width: 1, height: 2)
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.cornerRadius = 5
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layoutSubviews()

        containerView.layer.shadowPath = UIBezierPath(rect: containerView.bounds).cgPath
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let transform: CGAffineTransform = highlighted ? .highlightedCellScale : .identity
        UIView.animate(withDuration: CATransaction.animationDuration()) {
            self.containerView.transform = transform
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        guard selected else {
            return
        }

        UIView.animateKeyframes(withDuration: CATransaction.animationDuration() * 2, delay: 0, options: .beginFromCurrentState, animations: {

            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5) {
                self.containerView.transform = .highlightedCellScale
            }

            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.containerView.transform = .identity
            }
        }, completion: nil)
    }

}

extension EducationCell: Configurable {

    func configure(with model: EducationCellViewModel) {
        dateTitleLabel.text = .studyYearsTitle + ":"
        dateLabel.text = model.dateString
        departmentLabel.text = model.department
        codeLabel.text = "\(String.specialtyTitle) \(model.code)"
        specialtyLabel.text = model.specialty
    }
}
