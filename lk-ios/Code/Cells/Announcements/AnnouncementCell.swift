//
//  AnnouncementCell.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class AnnouncementCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var subjectLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var isReadImageView: UIImageView!
    @IBOutlet private weak var separatorView: SeparatorView!

    override func awakeFromNib() {
        super.awakeFromNib()

        isReadImageView.image = UIImage(color: .lkBlue, size: isReadImageView.bounds.size)
        isReadImageView.cornerRadius = isReadImageView.bounds.height / 2
    }
}

extension AnnouncementCell: Configurable {

    func configure(with model: AnnouncementCellViewModel) {
        subjectLabel.text = model.subject
        dateLabel.text = model.dateString
        isReadImageView.isHidden = model.isRead
        nameLabel.text = model.userName
        separatorView.isHidden = model.isLast
    }
}
