//
//  AnnouncementCellViewModel.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

struct AnnouncementCellViewModel {

    let subject: String
    let dateString: String
    let isRead: Bool
    let userName: String
    let isLast: Bool

    init(subject: String, date: Date, isRead: Bool, userName: String, isLast: Bool) {
        self.subject = subject
        self.dateString = DateFormatter.inAppEventDateFormatter.string(from: date)
        self.isRead = isRead
        self.userName = userName
        self.isLast = isLast
    }
}
