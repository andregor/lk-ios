//
//  ContactCellVIewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 27.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class ContactCellViewModel {

    let userId: Int
    let imageUrl: URL?
    let lastName: String
    let firstName: String
    let middleName: String
    private(set) var isSaved: Bool

    init(userId: Int, imageUrl: URL?, lastName: String, firstName: String, middleName: String, isSaved: Bool) {
        self.userId = userId
        self.imageUrl = imageUrl
        self.lastName = lastName
        self.firstName = firstName
        self.middleName = middleName
        self.isSaved = isSaved
    }

    func toggleSaved() {
        isSaved = !isSaved
    }
}
