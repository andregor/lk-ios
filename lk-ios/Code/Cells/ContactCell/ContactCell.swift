//
//  ContactCell.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 26.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class ContactCell: UITableViewCell {
    
    @IBOutlet private weak var profileImageView: UIImageView!
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let mask = CAShapeLayer()
        mask.path = UIBezierPath(ovalIn: profileImageView.bounds).cgPath
        profileImageView.layer.mask = mask
    }
}

extension ContactCell: Configurable {

    func configure(with model: ContactCellViewModel) {
        profileImageView.setImage(with: model.imageUrl, placeholderView: nil)
        nameLabel.text = model.lastName + " " + model.firstName + " " + model.middleName
    }
}
