//
//  TitleSubtitleCellViewModel.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class TitleSubtitleCellViewModel {

    let title: String
    let subtitle: String

    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
    }
}
