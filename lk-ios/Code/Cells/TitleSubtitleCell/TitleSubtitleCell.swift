//
//  TitleSubtitleCell.swift
//  lk-ios
//
//  Created by Egor Snitsar on 23/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class TitleSubtitleCell: UITableViewCell {
    
}

extension TitleSubtitleCell: Configurable {

    func configure(with model: TitleSubtitleCellViewModel) {
        textLabel?.text = model.title
        detailTextLabel?.text = model.subtitle
    }
}
