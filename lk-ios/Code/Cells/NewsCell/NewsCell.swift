//
//  NewsCell.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

final class NewsCell: UITableViewCell {

    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
}

extension NewsCell: Configurable {

    func configure(with model: NewsCellViewModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        thumbnailImageView.setImage(with: model.imagePath, placeholderView: nil)
    }
}
