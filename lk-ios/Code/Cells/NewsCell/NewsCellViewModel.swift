//
//  NewsCellViewModel.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

final class NewsCellViewModel {

    let imagePath: String
    let title: String
    let subtitle: String
    let newsPath: String
    
    init(imagePath: String, title: String, subtitle: String, newsPath: String) {
        self.imagePath = imagePath
        self.title = title
        self.subtitle = subtitle
        self.newsPath = newsPath
    }
}
