//
//  AnnouncementsService.swift
//  lk-ios
//
//  Created by Egor Snitsar on 14/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol AnnouncementsServiceMonitor {
    func didLoad(announcement: AnnouncementEntity)
    func didLoad(announcementsPage: PagedEntity<AnnouncementEntity>)
    func didFail(with error: Error)
}

final class AnnouncementsService: PagedService, Observable {

    typealias Request = AnnouncementsRequest

    private(set) var pages = [PagedEntity<AnnouncementEntity>]()

    private(set) var observers = ObserversArray<AnnouncementsServiceMonitor>()

    let pageSize: Int
    var totalPagesCount: Int = 1
    var isLoading: Bool = false

    let client: LKClient

    init(client: LKClient, pageSize: Int = 20) {
        self.client = client
        self.pageSize = pageSize
    }

    func onLoad(_ page: PagedEntity<AnnouncementEntity>, refreshed: Bool) {
        if refreshed {
            pages = [page]
        } else {
            pages.append(page)
        }

        observers.invoke { $0.didLoad(announcementsPage: page) }
    }

    func onError(_ error: Error) {
        observers.invoke { $0.didFail(with: error) }
    }
}

extension AnnouncementsService {

    func loadAnnouncement(with id: Int) {
        let request = AnnouncementRequest(id: id)
        client.send(request: request) {
            [weak self] result in

            switch result {
            case .success(value: let value):
                if let announcement = value {
                    self?.observers.invoke { $0.didLoad(announcement: announcement) }
                }
            case .failure(error: let error):
                self?.observers.invoke { $0.didFail(with: error) }
            }
        }
    }

    func indexPath(for announcement: AnnouncementEntity) -> IndexPath? {
        var rowIndex: Int?
        let sectionIndex = pages.index {
            page -> Bool in

            if let index = page.items.index(where: { $0.id == announcement.id }) {
                rowIndex = index
            }

            return rowIndex != nil
        }

        guard let row = rowIndex, let section = sectionIndex else {
            return nil
        }

        return IndexPath(row: row, section: section)
    }
}
