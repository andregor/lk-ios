//
//  MessagesService.swift
//  lk-ios
//
//  Created by Egor Snitsar on 06/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol MessagesServiceMonitor {
    func didLoadMessages()
    func didFail(with error: Error)
}

final class MessagesService: Observable {

    var messages: [MessageEntity] {
        return innerService.pages.flatMap { $0.items }
    }

    private(set) var isLoading: Bool = false
    private var completion: (() -> Void)?

    private let innerService: MessagesServiceInner

    private(set) var observers = ObserversArray<MessagesServiceMonitor>()

    init(client: LKClient) {
        innerService = MessagesServiceInner(client: client)
        innerService.add(observer: self)
    }

    func load(completion: (() -> Void)? = nil) {
        isLoading = true
        self.completion = completion
        innerService.load(refresh: true)
    }
}

extension MessagesService: MessagesServiceInnerMonitor {

    func didLoad(messagesPage: PagedEntity<MessageEntity>) {
        if innerService.isFullyLoaded {
            isLoading = false
            observers.invoke { $0.didLoadMessages() }
            completion?()
            return
        }

        innerService.load()
    }

    func didFail(with error: Error) {
        observers.invoke { $0.didFail(with: error) }
    }
}

private protocol MessagesServiceInnerMonitor {
    func didLoad(messagesPage: PagedEntity<MessageEntity>)
    func didFail(with error: Error)
}

private final class MessagesServiceInner: PagedService, Observable {
    
    typealias Request = MessagesRequest
    
    private(set) var pages = [PagedEntity<MessageEntity>]()
    
    private(set) var observers = ObserversArray<MessagesServiceInnerMonitor>()
    
    var pageSize: Int
    var totalPagesCount: Int = 1
    var isLoading: Bool = false
    
    let client: LKClient
    
    init(client: LKClient, pageSize: Int = 50) {
        self.client = client
        self.pageSize = pageSize
    }
    
    func onLoad(_ page: PagedEntity<MessageEntity>, refreshed: Bool) {
        if refreshed {
            pages = [page]
        } else {
            pages.append(page)
        }
        
        observers.invoke { $0.didLoad(messagesPage: page) }
    }
    
    func onError(_ error: Error) {
        observers.invoke { $0.didFail(with: error) }
    }
}
