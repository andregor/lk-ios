//
//  GradesService.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 16.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol GradesServiceMonitor {

    func didLoad(groups: [GradeGroup])
    func didFail(error: Error)
}

final class GradesService: Observable {

    private(set) var observers = ObserversArray<GradesServiceMonitor>()

    private let client: LKClient

    init(client: LKClient) {
        self.client = client
    }
    
}

// MARK: - Requests

extension GradesService {

    func getGradeGroups(educationId: Int) {
        let request = GradesRequest(educationId: educationId)

        client.send(request: request) { [weak self] result in
            switch result {
            case .success(value: let groups):
                self?.observers.invoke { $0.didLoad(groups: groups ?? []) }
            case .failure(error: let error):
                self?.observers.invoke { $0.didFail(error: error) }
            }
        }
    }
}
