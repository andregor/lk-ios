//
//  UserService.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 14.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol UserServiceMonitor {
    func didUpdateUser()
    func didFail(with error: Error)
    func didLogout()
}

final class UserService: Observable {
    
    private(set) var currentUser: ExtendedUser?

    private(set) var observers = ObserversArray<UserServiceMonitor>()

    private let client: LKClient
    
    init(client: LKClient) {
        self.client = client
    }
}

// MARK: - Requests

extension UserService {

    private struct Constants {
        static let currentUserId = 0
    }

    func loadCurrentUser(completion: (() -> Void)? = nil) {
        client.send(request: UserRequest(userId: Constants.currentUserId)) { [weak self] result in
            switch result {
            case .success(let user):
                guard let user = user else {
                    return
                }

                self?.currentUser = user
                self?.observers.invoke { $0.didUpdateUser() }
                
            case .failure(let error):
                self?.observers.invoke { $0.didFail(with: error) }
            }

            completion?()
        }
    }

    func logout() {
        client.send(request: LogoutRequest()) { [weak self] result in
            switch result {
            case .success(value: _):
                self?.observers.invoke { $0.didLogout() }
            case .failure(error: _):
                break
            }
        }
    }
}
