//
//  NewsService.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 02.05.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

protocol NewsServiceMonitor {
    func didLoad(news: NewsService.DataType)
    func didFail(with error: Error)
}

final class NewsService: Observable {

    typealias DataType = NewsRequest.Response
    typealias ObserverType = NewsServiceMonitor

    private(set) var observers = ObserversArray<NewsServiceMonitor>()

    private let client: LKClient

    init(client: LKClient) {
        self.client = client
    }
}

// MARK: - Requests

extension NewsService {

    func getNews() {
        client.send(request: NewsRequest()) {
            [weak self] result in

            switch result {
            case .success(value: let news):
                self?.observers.invoke { $0.didLoad(news: news ?? []) }

            case .failure(error: let error):
                self?.observers.invoke { $0.didFail(with: error) }
            }
        }
    }
}
