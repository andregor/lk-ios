//
//  ChatService.swift
//  lk-ios
//
//  Created by Egor Snitsar on 12/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol ChatServiceMonitor: class {
    func didLoad(messagesPage: PagedEntity<MessageEntity>)
    func didFail(with error: Error)

    func didUploadMessage(_ message: String)
    func didFailUploadingMessage(_ message: String, error: Error)
}

final class ChatService: PagedService, Observable {

    typealias Request = MessageHistoryRequest

    private(set) var pages = [PagedEntity<MessageEntity>]()

    private(set) var observers = ObserversArray<ChatServiceMonitor>()

    let pageSize: Int
    var totalPagesCount: Int = 1
    var isLoading: Bool = false

    let client: LKClient

    private let userId: Int

    required init(userId: Int, client: LKClient, pageSize: Int = 30) {
        self.userId = userId
        self.client = client
        self.pageSize = pageSize
    }

    convenience init(client: LKClient, pageSize: Int) {
        self.init(userId: 0, client: client, pageSize: pageSize)
    }

    var requestCreationClosure: (Int, Int) -> MessageHistoryRequest {
        return { MessageHistoryRequest(userId: self.userId, currentPage: $0, pageSize: $1) }
    }

    func onLoad(_ page: PagedEntity<MessageEntity>, refreshed: Bool) {
        if refreshed {
            pages = [page]
        } else {
            pages.append(page)
        }

        observers.invoke { $0.didLoad(messagesPage: page) }
    }

    func onError(_ error: Error) {
        observers.invoke { $0.didFail(with: error) }
    }
}

extension ChatService {

    func send(message: String) {
        let request = SendMessageRequest(userId: userId, text: message)
        client.send(request: request) {
            [weak self] result in

            switch result {
            case .success:
                self?.observers.invoke { $0.didUploadMessage(message) }

            case .failure(error: let error):
                self?.observers.invoke { $0.didFailUploadingMessage(message, error: error) }
            }
        }
    }
}
