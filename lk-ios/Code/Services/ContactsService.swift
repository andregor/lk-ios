//
//  ContactsService.swift
//  lk-ios
//
//  Created by Andrey Ovsyannikov on 12.06.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

protocol ContactsServiceMonitor {

    func didLoad(contacts: [BaseUser], refreshed: Bool)
    func didFail(error: Error)
}

final class ContactsService: Observable {

    private(set) var pages = [PagedEntity<BaseUser>]()
    private(set) var observers = ObserversArray<ContactsServiceMonitor>()

    let pageSize: Int
    var totalPagesCount: Int = 1
    var isLoading: Bool = false
    
    let client: LKClient
    var requestCreationClosure: (Int, Int) -> Request

    init(client: LKClient, pageSize: Int = 20) {
        self.client = client
        self.pageSize = pageSize
        self.requestCreationClosure = {
            return ContactsRequest(currentPage: $0, pageSize: $1)
        }
    }
}

extension ContactsService: PagedService {

    typealias Request = ContactsRequest

    func load(lastName: String, refresh: Bool) {
        requestCreationClosure = {
            ContactsRequest(lastName: lastName, currentPage: $0, pageSize: $1)
        }

        load(refresh: refresh)
    }

    func onLoad(_ page: PagedEntity<BaseUser>, refreshed: Bool) {
        if refreshed {
            pages = [page]
        } else {
            pages.append(page)
        }

        observers.invoke { $0.didLoad(contacts: page.items, refreshed: refreshed) }
    }

    func onError(_ error: Error) {
        observers.invoke { $0.didFail(error: error) }
    }
}

extension ContactsService {

    func user(with id: Int) -> BaseUser? {
        return pages.flatMap({ $0.items }).first { $0.id == id }
    }
}
