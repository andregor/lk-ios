//
//  AppDelegate.swift
//  lk-ios
//
//  Created by Egor Snitsar on 25/03/18.
//  Copyright © 2018 andregor. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var applicationModule: ApplicationModule?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        let window = UIWindow()
        let applicationModule = ApplicationModule()
        applicationModule.assemble(with: window)
        applicationModule.showInitialModule()
        self.window = window
        self.applicationModule = applicationModule
        
        return true
    }
}
