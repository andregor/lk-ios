//
//  Serializer.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 09.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

open class RequestSerializer {
    
    public init() {}

    open func serialize<T: Request>(request: T, for client: HTTPClient) throws -> URLRequest {
        let urlString = client.configuration.baseUrl + request.urlPath
        guard let url = URL(string: urlString) else {
            throw NetworkError.serializationError(reason: "Could not form URL")
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.timeoutInterval = request.timeoutInterval ?? client.configuration.timeoutInterval

        client.configuration.headers.forEach {
            urlRequest.setValue($0.value, forHTTPHeaderField: $0.key)
        }

        request.requestHeaders.forEach {
            urlRequest.addValue($0.value, forHTTPHeaderField: $0.key)
        }

        do {
            try request.parametersEncoder.encode(parameters: request.parameters, to: &urlRequest)
        } catch {
            throw NetworkError.serializationError(reason: "Could not encode parameters")
        }

        return urlRequest
    }
}
