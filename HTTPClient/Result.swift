//
//  Result.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 09.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public enum Result<T> {

    case success(value: T?)
    case failure(error: Error)
}
