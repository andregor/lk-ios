//
//  ParametersEncoding.swift
//  HTTPClient
//
//  Created by Egor Snitsar on 13/05/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public protocol ParametersEncoder {
    func encode(parameters: [String: Any]?, to request: inout URLRequest) throws
}

public class QueryEncoder {

    public init() {}

    func encode(parameters: [String : Any]?, with url: URL) throws -> URLComponents? {
        guard let parameters = parameters, !parameters.isEmpty else {
            return nil
        }

        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            throw NetworkError.serializationError(reason: "Unable to retrieve url components")
        }

        let queryItems = parameters.flatMap(queryComponents)
        if urlComponents.queryItems == nil || urlComponents.queryItems?.isEmpty == true {
            urlComponents.queryItems = queryItems
        } else {
            urlComponents.queryItems?.append(contentsOf: queryItems)
        }

        return urlComponents
    }

    func queryComponents(key: String, value: Any) -> [URLQueryItem] {
        var components: [URLQueryItem] = []

        if let dict = value as? [String: Any] {
            for (nestedKey, value) in dict {
                components += queryComponents(key: "\(key)[\(nestedKey)]", value: value)
            }
        } else if let array = value as? [Any] {
            for value in array {
                components += queryComponents(key: "\(key)[]", value: value)
            }
        } else if let bool = value as? Bool {
            components.append(URLQueryItem(name: key, value: bool ? "1" : "0"))
        } else if let val = value as? OptionalProtocol, val.isSome {
            components.append(URLQueryItem(name: key, value: "\(val.value)"))
        } else {
            components.append(URLQueryItem(name: key, value: "\(value)"))
        }

        return components
    }
}

public final class URLParametersEncoder: QueryEncoder, ParametersEncoder {

    public func encode(parameters: [String : Any]?, to request: inout URLRequest) throws {
        guard let url = request.url else {
            throw NetworkError.serializationError(reason: "Unable to retrieve request's url")
        }

        guard let urlComponents = try encode(parameters: parameters, with: url) else {
            return
        }

        if let resultURL = urlComponents.url {
            request.url = resultURL
        }
    }
}

public final class URLFormParametersEncoder: QueryEncoder, ParametersEncoder {

    public func encode(parameters: [String : Any]?, to request: inout URLRequest) throws {
        guard let url = request.url else {
            throw NetworkError.serializationError(reason: "Unable to retrieve request's url")
        }

        guard var urlComponents = try encode(parameters: parameters, with: url) else {
            return
        }

        if let resultUrl = URL(string: urlComponents.percentEncodedQuery ?? "") {
            request.httpBody = resultUrl.dataRepresentation
        }
    }

    override func queryComponents(key: String, value: Any) -> [URLQueryItem] {
        var components: [URLQueryItem] = []

        if let dict = value as? [String: Any] {
            for (nestedKey, value) in dict {
                components += queryComponents(key: "\(key)[\(nestedKey)]", value: value)
            }
        } else if let array = value as? [Any] {
            array.enumerated().forEach { index, value in
                components += queryComponents(key: "\(key)[\(index)]", value: value)
            }
        } else if let bool = value as? Bool {
            components.append(URLQueryItem(name: key, value: bool ? "true" : "false"))
        } else if let val = value as? OptionalProtocol, val.isSome {
            components.append(URLQueryItem(name: key, value: "\(val.value)"))
        } else {
            components.append(URLQueryItem(name: key, value: "\(value)"))
        }

        return components
    }
}

public final class JSONParametersEncoder: ParametersEncoder {

    private let encodingOptions: JSONSerialization.WritingOptions

    public init(encodingOptions: JSONSerialization.WritingOptions = []) {
        self.encodingOptions = encodingOptions
    }

    public func encode(parameters: [String : Any]?, to request: inout URLRequest) throws {
        guard let parameters = parameters else {
            return
        }

        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: encodingOptions)

            if request.value(forHTTPHeaderField: "Content-Type") == nil {
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            }

            request.httpBody = data
        } catch {
            throw NetworkError.serializationError(reason: "Failed to serialize request's parameters to JSON")
        }
    }
}

public final class MultipartFormDataParametersEncoder: ParametersEncoder {

    public init() {}

    public func encode(parameters: [String : Any]?, to request: inout URLRequest) throws {
        guard let parameters = parameters else {
            return
        }

        let boundary = "----Boundary-\(UUID().uuidString)"

        if request.value(forHTTPHeaderField: "Content-Type") == nil {
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        }

        var bodyString = ""

        let boundaryPrefix = "--\(boundary)\r\n"

        for (key, value) in parameters {
            bodyString.append(contentsOf: boundaryPrefix)
            bodyString.append(contentsOf: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            bodyString.append(contentsOf: "\(value)\r\n")
        }

        bodyString.append(contentsOf: "--\(boundary)--")

        request.httpBody = bodyString.data(using: .utf8)
    }
}
