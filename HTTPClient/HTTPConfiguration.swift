//
//  HTTPConfiguration.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 08.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public enum SessionType {
    case `default`
    case background(identifier: String)
}

public struct HTTPConfiguration {

    public let baseUrl: String
    public let timeoutInterval: TimeInterval
    public let sessionType: SessionType
    public let acceptableStatusCodes: Set<Int>
    public let headers: [String: String]
    public let completionQueue: DispatchQueue
    public let requestSerializer: RequestSerializer

    public init(baseUrl: String,
         timeoutInterval: TimeInterval = 20,
         sessionType: SessionType = .default,
         acceptableStatusCodes: Set<Int> = Set(200 ..< 300),
         headers: [String: String] = [:],
         completionQueue: DispatchQueue = DispatchQueue.main,
         requestSerializer: RequestSerializer = RequestSerializer()) {

        self.baseUrl = baseUrl
        self.timeoutInterval = timeoutInterval
        self.sessionType = sessionType
        self.acceptableStatusCodes = acceptableStatusCodes
        self.headers = headers
        self.completionQueue = completionQueue
        self.requestSerializer = requestSerializer
    }
}
