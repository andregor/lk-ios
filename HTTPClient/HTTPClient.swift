//
//  HTTPClient.swift
//  HTTPClient
//
//  Created by Andrey Ovsyannikov on 08.04.2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public typealias CompletionHandler<T> = (Result<T>) -> ()

open class HTTPClient {

    public let urlSession: URLSession
    public let configuration: HTTPConfiguration

    public init(configuration: HTTPConfiguration) {
        let urlConfiguration: URLSessionConfiguration
        switch configuration.sessionType {
        case .default:
            urlConfiguration = .default
        case .background(identifier: let identifier):
            urlConfiguration = .background(withIdentifier: identifier)
        }

        urlSession = URLSession(configuration: urlConfiguration)
        self.configuration = configuration
    }
    
    open func proccessResponse<T: Request>(data: Data?,
                                           urlResponse: URLResponse?,
                                           error: Error?,
                                           for request: T) -> Result<T.Response> {
        
        do {
            try validate(response: urlResponse, data: data, error: error, request: request)
        } catch let error {
            return .failure(error: error)
        }
        
        guard let data = data else {
            return .success(value: nil)
        }
        
        return request.responseSerializer.serialize(data: data)
    }
    
    deinit {
        urlSession.invalidateAndCancel()
    }
}

// MARK: - Operations

extension HTTPClient {

    public func send<T: Request>(request: T, completion: @escaping CompletionHandler<T.Response>) {
        let urlRequest: URLRequest
        do {
            urlRequest = try configuration.requestSerializer.serialize(request: request, for: self)
        } catch let error {
            self.configuration.completionQueue.async {
                completion(.failure(error: error))
            }
            return
        }

        let task = urlSession.dataTask(with: urlRequest) { [weak self] data, urlResponse, error in
            guard let result = self?.proccessResponse(data: data, urlResponse: urlResponse, error: error, for: request) else {
                return
            }
            
            self?.configuration.completionQueue.async {
                completion(result)
            }
        }
        task.resume()
    }
}

private extension HTTPClient {

    func validate<T: Request>(response: URLResponse?, data: Data?, error: Error?, request: T) throws {
        if let error = error {
            throw error
        }

        if let urlResponse = response as? HTTPURLResponse, !configuration.acceptableStatusCodes.contains(urlResponse.statusCode) {
            throw NetworkError.httpError(statusCode: urlResponse.statusCode)
        }
    }
}
