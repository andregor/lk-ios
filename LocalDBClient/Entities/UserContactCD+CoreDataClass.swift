import Foundation
import CoreData

class UserContactCD : NSManagedObject, CoreDataFetchable {

    static let entityName: String = String(describing: UserContactCD.self)

    typealias PlainObjectType = UserData

    func fillFromPlainObject(_ plainObject: PlainObjectType) {
        addDate = Date()
        firstName = plainObject.firstName
        lastName = plainObject.lastName
        middleName = plainObject.middleName
        photoURL = plainObject.photoURL?.absoluteString
        localId = Int64(plainObject.userId)
    }

    func toPlainObject() -> PlainObjectType {
        return UserContact(userId: Int(localId),
                           firstName: firstName ?? "",
                           lastName: lastName ?? "",
                           middleName: middleName ?? "",
                           photoURL: URL(string: photoURL ?? ""))
    }
}
