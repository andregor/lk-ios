//
//  NSManagedObject+Uniqueness.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

internal extension CoreDataFetchable where Self: NSManagedObject {

    typealias UniqueKeyValueType = (key: String, value: Int)

    static func defaultUniquePredicate(for id: Int) -> NSPredicate {
        return NSPredicate(format: "localId == %d", id)
    }
}

