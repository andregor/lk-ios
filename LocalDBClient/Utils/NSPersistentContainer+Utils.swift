//
//  NSPersistentContainer+Utils.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

extension NSPersistentContainer {

    public func resetStore() {
        guard let objectModel = viewContext.persistentStoreCoordinator?.managedObjectModel else {
            return
        }

        let myEntities = objectModel.entities
        for entity in myEntities {
            if let name = entity.name {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: name)
                let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

                do {
                    try viewContext.execute(deleteRequest)
                    try viewContext.save()
                } catch {
                    print(error)
                }
            }
        }
    }
}
