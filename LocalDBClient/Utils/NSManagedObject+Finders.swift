//
//  NSManagedObject+Finders.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

/**
 Protocol to be conformed to by `NSManagedObject` subclasses that allow for convenience
 methods that make fetching, inserting, deleting, and change management easier.
 */
@objc protocol CoreDataFetchable: NSFetchRequestResult {
    static var entityName: String { get }
}

extension CoreDataFetchable where Self: NSManagedObject {

    static func entityDescriptionForFetchable(in context: NSManagedObjectContext) -> NSEntityDescription! {
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: context) else {
            assertionFailure("Entity named \(entityName) doesn't exist. Fix the entity description or naming of \(Self.self).")
            return nil
        }
        return entity
    }

    static func fetchRequestForEntityInFetchable(inContext context: NSManagedObjectContext) -> NSFetchRequest<Self> {
        let fetchRequest = NSFetchRequest<Self>()
        fetchRequest.entity = entityDescriptionForFetchable(in: context)
        return fetchRequest
    }

    static func executeFetchRequest(_ request: NSFetchRequest<Self>, inContext context: NSManagedObjectContext) -> [Self] {

        var results: [Self] = []

        context.performAndWait {
            do {
                let anyArray = try context.fetch(request)
                results = anyArray

            } catch {
                debugPrint("Couldn't create entity \(entityName) in context with concurrency type \(context.concurrencyType), error \(error)")
            }
        }

        return results
    }

    static func findAllInContext(_ context: NSManagedObjectContext) -> [Self] {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)

        return executeFetchRequest(fetchRequest, inContext: context)
    }

    static func findAllWithPredicate(_ searchTerm: NSPredicate, inContext context: NSManagedObjectContext) -> [Self] {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)

        fetchRequest.predicate = searchTerm

        return executeFetchRequest(fetchRequest, inContext: context)
    }

    static func findFirstWithPredicate(_ searchTerm: NSPredicate, inContext context: NSManagedObjectContext) -> Self? {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)

        fetchRequest.predicate = searchTerm

        return executeFetchRequest(fetchRequest, inContext: context).first
    }

    static func findAllSortedBy(_ sortTerm: String, ascending: Bool, inContext context: NSManagedObjectContext) -> [Self] {
        return findAllSortedBy(sortTerm, ascending: ascending, withPredicate: nil, inContext: context)
    }

    static func findAllSortedBy(_ sortTerm: String, ascending: Bool, withPredicate predicate: NSPredicate?, inContext context: NSManagedObjectContext) -> [Self] {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)

        fetchRequest.predicate = predicate

        let sortKeys = sortTerm.components(separatedBy: ",")

        var sortDesciptors = [NSSortDescriptor]()
        for sortKey in sortKeys {
            let sortDescriptor = NSSortDescriptor(key: sortKey, ascending: ascending)

            sortDesciptors.append(sortDescriptor)
        }

        fetchRequest.sortDescriptors = sortDesciptors

        return executeFetchRequest(fetchRequest, inContext: context)
    }

    static func count(with predicate: NSPredicate?, in context: NSManagedObjectContext) -> Int {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)
        fetchRequest.predicate = predicate
        fetchRequest.includesSubentities = false

        var count = 0
        context.performAndWait {
            do {
                count = try context.count(for: fetchRequest)
            } catch {
                debugPrint("Couldn't count entity \(entityName) in context with concurrency type \(context.concurrencyType), error \(error)")
            }
        }

        return count
    }

    static func batchDelete(with predicate: NSPredicate?, in context: NSManagedObjectContext) {
        let fetchRequest = fetchRequestForEntityInFetchable(inContext: context)
        fetchRequest.predicate = predicate

        // swiftlint:disable:next force_cast
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)

        do {
            try context.execute(deleteRequest)
        } catch {
            debugPrint("Couldn't batch delete entity \(entityName) in context with concurrency type \(context.concurrencyType), error \(error)")
        }
    }

    static func batchUpdate(with predicate: NSPredicate?, propertiesToUpdate: [AnyHashable: Any], in context: NSManagedObjectContext) {
        let updateRequest = NSBatchUpdateRequest(entity: entityDescriptionForFetchable(in: context))
        updateRequest.predicate = predicate
        updateRequest.propertiesToUpdate = propertiesToUpdate

        do {
            try context.execute(updateRequest)
        } catch {
            debugPrint("Couldn't batch update entity \(entityName) in context with concurrency type \(context.concurrencyType), error \(error)")
        }
    }

    func entityInContext(_ otherContext: NSManagedObjectContext) -> Self? {
        if objectID.isTemporaryID {
            do {
                try managedObjectContext?.obtainPermanentIDs(for: [self])
            } catch {
                debugPrint(error)
                return nil
            }
        }

        do {
            let inContext = try otherContext.existingObject(with: objectID)
            return inContext as? Self
        } catch {
            debugPrint(error)
        }

        return nil
    }
}

