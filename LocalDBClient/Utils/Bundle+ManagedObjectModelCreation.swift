//
//  Bundle+ManagedObjectModelCreation.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 18/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

extension Bundle {

    public func managedObjectModel(name: String) -> NSManagedObjectModel {
        guard let URL = url(forResource: name, withExtension: "momd"),
            let model = NSManagedObjectModel(contentsOf: URL) else {
                preconditionFailure("Model not found or corrupted with name: \(name) in bundle: \(self)")
        }
        return model
    }
}
