//
//  UserContact.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import Foundation

public protocol UserData {

    var userId: Int { get }
    var firstName: String { get }
    var lastName: String { get }
    var middleName: String? { get }
    var photoURL: URL? { get }
}

public struct UserContact: UserData {

    public let userId: Int
    public let firstName: String
    public let lastName: String
    public let middleName: String?
    public let photoURL: URL?

    public init(userId: Int, firstName: String, lastName: String, middleName: String?, photoURL: URL?) {
        self.userId = userId
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.photoURL = photoURL
    }
}
