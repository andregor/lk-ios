//
//  NSManagedObject+Creation.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

internal extension CoreDataFetchable where Self: NSManagedObject {

    internal init(managedObjectContext context: NSManagedObjectContext) {
        self.init(entity: Self.entityDescriptionForFetchable(in: context), insertInto: context)
    }

    internal static func createEntityInContext(_ context: NSManagedObjectContext) -> Self {

        let entityDesc = NSEntityDescription.entity(forEntityName: entityName, in: context)

        guard let entity = entityDesc else {
            fatalError("Not found entity description for \(entityName) in \(context)")
        }

        return self.init(entity: entity, insertInto: context)
    }

    @discardableResult
    internal static func lazyCreateEntityInContext(_ context: NSManagedObjectContext, withPredicate predicate: NSPredicate?) -> Self {

        let result: Self?
        if let predicate = predicate {
            result = findAllWithPredicate(predicate, inContext: context).first
        } else {
            result = findAllInContext(context).first
        }

        return result ?? createEntityInContext(context)
    }

    internal func deleteEntity() {
        managedObjectContext?.delete(self)
    }

    private func deleteEntitesSet(_ items: Set<NSManagedObject>) {
        for item in items {
            item.managedObjectContext?.delete(item)
        }
    }

    internal func deleteEntities(_ items: inout NSSet?) {
        if let items = items as? Set<NSManagedObject> {
            deleteEntitesSet(items)
        }
        items = nil
    }

    internal func deleteEntities(_ items: inout NSOrderedSet?) {
        if let items = items?.set as? Set<NSManagedObject> {
            deleteEntitesSet(items)
        }
        items = nil
    }
}
