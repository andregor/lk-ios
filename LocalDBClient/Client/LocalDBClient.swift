//
//  LocalDBClient.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

public class LocalDBClient {

    internal var container: NSPersistentContainer!

    fileprivate let databaseNotificator: DatabaseNotificator

    public init() {
        databaseNotificator = DatabaseNotificator()
    }

    public func setupCoreDataStack(completion: (() -> Void)?) {
        let model = Bundle(for: type(of: self)).managedObjectModel(name: Services.modelName)
        let container = NSPersistentContainer(name: Services.modelName, managedObjectModel: model)
        container.loadPersistentStores {
            [weak self] storeDescription, error in

            if let error = error {
                NSException.raise(NSExceptionName(rawValue: "Core Data exception"),
                                  format: "Core data stack was initialized with error \(error)",
                    arguments: getVaList([]))
            } else {
                self?.container = container
                self?.setupServices()
                self?.databaseNotificator.setupObservers(context: container.viewContext, frequency: .onSave)
                completion?()
            }
        }
    }

    //Services to work with local data.
    //They are shared, so don't store them strongly.

    public var contactsService: LocalContactsService!

    fileprivate func setupServices() {
        contactsService = setupService()
    }

    private func setupService<Service: BaseService>() -> Service! {
        let service = Service(context: container.viewContext)
        databaseNotificator.observersMap[Service.observableEntityName] = service
        return service
    }

    public func resetStore() {
        container.resetStore()
    }
}

private struct Services {
    static let modelName = "lk-ios"
}
