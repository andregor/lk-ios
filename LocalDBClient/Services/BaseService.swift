//
//  BaseService.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

public class BaseService {

    unowned let context: NSManagedObjectContext

    public required init(context: NSManagedObjectContext) {
        self.context = context
    }

    /**
     Callback for when objects matching the predicate have been inserted

     - parameter entities: The set of inserted matching objects
     */
    func entityMonitorObservedInserts(entities: Set<NSManagedObject>) {
        entityMonitorObservedUpdates(entities: entities)
    }

    /**
     Callback for when objects matching the predicate have been deleted

     - parameter entities: The set of deleted matching objects
     */
    func entityMonitorObservedDeletions(entities: Set<NSManagedObject>) {

    }

    /**
     Callback for when objects matching the predicate have been updated

     - parameter entities: The set of updated matching objects
     */
    func entityMonitorObservedModifications(entities: Set<NSManagedObject>) {
        entityMonitorObservedUpdates(entities: entities)
    }

    /**
     Function that will be called on entities inserts and modifications.
     Usually you want to cast NSManagedObjects to specific type, that this service is observing.

     - parameter entities: The set of updated matching objects
     */
    func entityMonitorObservedUpdates(entities: Set<NSManagedObject>) {

    }

    /**
     Name of entity this service want to observe.
     */
    //It's possible to implement
    class var observableEntityName: String {
        fatalError("observableEntityName not implemented")
    }
}
