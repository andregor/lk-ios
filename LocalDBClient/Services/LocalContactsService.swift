//
//  LocalContactsService.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

public protocol LocalContactsServiceMonitor: class {

    typealias ObservableType = UserData

    func entityMonitorObservedModifications(entities: [ObservableType])
    func entityMonitorObservedInserts(entities: [ObservableType])
    func entityMonitorObservedDeletions(entities: [ObservableType])
}

public class LocalContactsService: BaseService, Observable {

    typealias ObservableType = UserContactCD

    private var contactsCD: [ObservableType] {
        return ObservableType.findAllSortedBy("addDate", ascending: false, inContext: context)
    }

    public var contacts: [UserData] {
        return self.castedEntities(from: Set(contactsCD))
    }

    public var isEmpty: Bool {
        return ObservableType.count(with: nil, in: context) == 0
    }

    public func hasContact(_ contact: UserData) -> Bool {
        return ObservableType.count(with: ObservableType.defaultUniquePredicate(for: contact.userId), in: context) != 0
    }

    public func toggle(contact: UserData) {
        if let entity = ObservableType.findFirstWithPredicate(ObservableType.defaultUniquePredicate(for: contact.userId), inContext: context) {
            entity.deleteEntity()
        } else {
            let entity = ObservableType.createEntityInContext(context)
            entity.fillFromPlainObject(contact)
        }
        try? context.save()
    }

    public let observers = ObserversArray<LocalContactsServiceMonitor>()

    override class var observableEntityName: String {
        return ObservableType.entityName
    }

    override func entityMonitorObservedInserts(entities: Set<NSManagedObject>) {
        let castedEntities = self.castedEntities(from: entities)
        observers.invoke { $0.entityMonitorObservedInserts(entities: castedEntities) }
    }

    override func entityMonitorObservedDeletions(entities: Set<NSManagedObject>) {
        let castedEntities = self.castedEntities(from: entities)
        observers.invoke { $0.entityMonitorObservedDeletions(entities: castedEntities) }
    }

    override func entityMonitorObservedModifications(entities: Set<NSManagedObject>) {
        let castedEntities = self.castedEntities(from: entities)
        observers.invoke { $0.entityMonitorObservedModifications(entities: castedEntities) }
    }

    private func castedEntities(from entities: Set<NSManagedObject>) -> [LocalContactsServiceMonitor.ObservableType] {
        return entities.compactMap { ($0 as? ObservableType)?.toPlainObject() }
    }
}
