//
//  DatabaseNotificator.swift
//  LocalDBClient
//
//  Created by Egor Snitsar on 17/06/2018.
//  Copyright © 2018 andregor. All rights reserved.
//

import CoreData

enum FireFrequency {
    /// Notifications will be sent upon `NSManagedObjectContext` being changed
    case onChange

    /// Notifications will be sent upon `NSManagedObjectContext` being saved
    case onSave
}

final class DatabaseNotificator {

    typealias EntitySet = Set<NSManagedObject>

    weak var context: NSManagedObjectContext?

    var observersMap: [String: BaseService] = [:]

    deinit {
        removeObservers()
    }

    final func setupObservers(context: NSManagedObjectContext, frequency: FireFrequency) {

        self.context = context

        let notificationName: NSNotification.Name
        switch frequency {
        case .onChange:
            notificationName = .NSManagedObjectContextObjectsDidChange
        case .onSave:
            notificationName = .NSManagedObjectContextDidSave
        }

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onChangeNotification(_:)),
                                               name: notificationName,
                                               object: context)
    }

    final func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc final func onChangeNotification(_ notification: Notification) {
        guard let changeSet = notification.userInfo as? [String: Any] else {
            return
        }

        context?.performAndWait {

            func process(_ value: Any?) -> EntitySet {
                let entitySet = value as? EntitySet
                return entitySet ?? []
            }

            let inserted = process(changeSet[NSInsertedObjectsKey])
            let deleted = process(changeSet[NSDeletedObjectsKey])
            let updated = process(changeSet[NSUpdatedObjectsKey])
            self.handleChanges(inserted: inserted, deleted: deleted, updated: updated)
        }
    }

    func handleChanges(inserted: EntitySet, deleted: EntitySet, updated: EntitySet) {
        let insertedMapped: [String: EntitySet] = makeMappedEntitySet(entities: inserted)
        let deletedMapped = makeMappedEntitySet(entities: deleted)
        let updatedMapped = makeMappedEntitySet(entities: updated)
        for (name, entities) in insertedMapped {
            if let observer = observersMap[name] {
                observer.entityMonitorObservedInserts(entities: entities)
            }
        }

        for (name, entities) in deletedMapped {
            if let observer = observersMap[name] {
                observer.entityMonitorObservedDeletions(entities: entities)
            }
        }

        for (name, entities) in updatedMapped {
            if let observer = observersMap[name] {
                observer.entityMonitorObservedModifications(entities: entities)
            }
        }
    }

    private func makeMappedEntitySet(entities: EntitySet) -> [String: EntitySet] {
        var mappedEntities: [String: EntitySet] = [:]
        for entity in entities {
            if let name = entity.entity.name {
                if mappedEntities[name] == nil {
                    mappedEntities[name] = EntitySet()
                }
                mappedEntities[name]?.insert(entity)
            }
        }

        return mappedEntities
    }
}
